﻿using API_LoL_Project.utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace API_LoL_Project.Middleware.Auth
{
    public class AuthMiddlewareFliter : IAsyncAuthorizationFilter
    {
        private readonly IConfiguration _configuration;

        public AuthMiddlewareFliter(IConfiguration configuration)
        {
            this._configuration = configuration;
        }
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (!context.HttpContext.Request.Headers.TryGetValue(AuthUtils.ApiKeyHeaderName, out var clientApiKey))
            {
                context.Result = new UnauthorizedObjectResult("ApiKey is missing please ask author's permission");
                return;
            }

            var apiKey = _configuration.GetValue<string>(AuthUtils.ApiKeySectionName);
            if (!apiKey.Equals(clientApiKey))
            {
                context.Result = new UnauthorizedObjectResult("ApiKey used is invalid");
                return;
            }

            // If the API key is valid, allow the request to proceed.
            await Task.CompletedTask;
        }
    }
}
