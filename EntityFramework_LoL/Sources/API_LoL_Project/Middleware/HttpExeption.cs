﻿using Microsoft.AspNetCore.Http.Headers;
using System.Net;

namespace API_LoL_Project.Middleware
{
    public class HttpExeption : Exception

    {

        public HttpStatusCode StatusCode { get; set; }
        public String Message { get; set; }


        public HttpExeption(HttpStatusCode statusCode, string message ="") : base(message)
        {
            this.StatusCode = statusCode;
            this.Message = message;
        }
        /*void AddHeaders(this HttpRequestMessage message, RequestHeaders headers)
        {
            var headerParameters = headers.Parameters.Where(x => !KnownHeaders.IsContentHeader(x.Name!));

            headerParameters.ForEach(x => AddHeader(x, message.Headers));
        }
        void AddHeader(Parameter parameter, HttpHeaders httpHeaders)
        {
            var parameterStringValue = parameter.Value!.ToString();

            httpHeaders.Remove(parameter.Name!);
            httpHeaders.TryAddWithoutValidation(parameter.Name!, parameterStringValue);
        }*/
    }
}
