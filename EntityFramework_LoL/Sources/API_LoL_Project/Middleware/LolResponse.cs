﻿using API_LoL_Project.Controllers.Response;
using System.Net;

namespace API_LoL_Project.Middleware
{
    public class LolResponse<T> 
    {
        public T Data { get; set; }
        public List<EndPointLink> Links { get; set; } = null;


        public LolResponse(T data , List<EndPointLink>? links)
        {
            this.Data = data;
            this.Links = links;
        }
    }
}
