﻿namespace API_LoL_Project.Controllers.Request
{
    public class PageRequest
    {
        public string? orderingPropertyName { get; set; } = null;
        public bool? descending { get; set; }  = false;
        public int index { get; set; } = 0;
        public int count { get; set; } = 0;


    }
}
