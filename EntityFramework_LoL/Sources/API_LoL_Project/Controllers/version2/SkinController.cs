﻿using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_LoL_Project.Controllers.version2
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("2.0")]
    [ApiController]
    public class SkinController : ControllerBase
    {
        /* public ISkinsManager dataManager;
         private readonly ILogger<ChampionsController> _logger;
         public SkinController(IDataManager dataManager, ILogger<ChampionsController> logger)
         {
             this.dataManager = dataManager.SkinsMgr;
             this._logger = logger;
         }

         // GET: api/<SkinController>
         [HttpGet]
         public async Task<ActionResult<IEnumerable<RuneDTO>>> Get([FromQuery] Request.PageRequest request)
         {
             try
             {
                 var totalcount = await dataManager.GetNbItems();
                 if (request.count + request.index > totalcount)
                 {
                     _logger.LogWarning("to many rows ask the max is {totalcount}", totalcount);
                     return BadRequest("to many rows ask the max is " + totalcount);
                 }
                 _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(Get), request);


                 var runes = await dataManager.GetItems(request.PageNumber, totalcount, request.orderingPropertyName, (request.descending == null ? false : (bool)request.descending));
                 IEnumerable<RuneDTO> res = runes.Select(c => c.toDTO());
                 if (res.Count() >= 0 || res == null)
                 {
                     _logger.LogWarning("No runes found with Id");
                     return BadRequest("No runes found with Id ");
                 }
                 return Ok(res);
             }
             catch (Exception e)
             {
                 _logger.LogError("About get at {e.message}", DateTime.UtcNow.ToLongTimeString());
                 return BadRequest(e.Message);

             }


         }

         // GET api/<SkinController>/5
         [HttpGet("{id}")]
         public string Get(int id)
         {
             return "value";
         }

         // POST api/<SkinController>
         [HttpPost]
         public void Post([FromBody] string value)
         {
         }

         // PUT api/<SkinController>/5
         [HttpPut("{id}")]
         public void Put(int id, [FromBody] string value)
         {
         }

         // DELETE api/<SkinController>/5
         [HttpDelete("{id}")]
         public void Delete(int id)
         {
         }*/
    }
}
