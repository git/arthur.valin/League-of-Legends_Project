﻿using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using API_LoL_Project.Controllers.Response;
using API_LoL_Project.Middleware;
using ApiMappeur;
using API_LoL_Project.Controllers.Response;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_LoL_Project.Controllers.version2
{

    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("2.0")]
    [ApiController]
    public class RuneController : ControllerBase
    {
        public IRunesManager dataManager;
        // you should create a custom logger to be prety
        private readonly ILogger<RuneController> _logger;


        public RuneController(IDataManager dataManager, ILogger<RuneController> logger)
        {
            this.dataManager = dataManager.RunesMgr;

            _logger = logger;
        }



        // GET: api/rune
        [HttpGet("/all")]
        public async Task<ActionResult<IEnumerable<RuneDTO>>> GetAllRunes([FromQuery] Request.PageRequest request)
        {
            try
            {
                var totalcount = await dataManager.GetNbItems();
                if (request.count * request.index >= totalcount)
                {
                    _logger.LogError("To many object is asked the max is {totalcount} but the request is supérior of ", totalcount);
                    return BadRequest("To many object is asked the max is : " + totalcount);
                }

                _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(GetAllRunes), request); ;
                var runes = await dataManager.GetItems(request.index, request.count, request.orderingPropertyName, (request.descending == null ? false : (bool)request.descending));
                IEnumerable<RuneDTO> res = runes.Select(c => c.ToDTO());
                if (res.Count() <= 0 || res == null)
                {
                    _logger.LogError("No runes found the total count is {totalcount} ", totalcount);
                    return BadRequest("No runes found : totalcount is : " + totalcount);
                }

                var respList = res.Select(r => new LolResponse<RuneDTO>
                (
                    r,
                    new List<EndPointLink>
                    {
                        EndPointLink.To($"/api/[controller]/{r.Name}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetAllRunes)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetAllRunes)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetAllRunes)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetAllRunes)}", "self","POST"),
                    }
                ));

                var pageResponse = new PageResponse<RuneDTO>(respList, request.index, request.count, totalcount);

                return Ok(pageResponse);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Rune controller : " + e.Message);
                return BadRequest(e.Message);

            }
    }
            


        // GET: api/<RuneController>
        [HttpGet]
         public async Task<ActionResult<IEnumerable<RuneDTO>>> Get([FromQuery] Request.PageRequest request)
         {
             try
             {
                 var totalcount = await dataManager.GetNbItems();
                 if (request.count + request.index > totalcount)
                 {
                     _logger.LogWarning("to many rows ask the max is {totalcount}", totalcount);
                     return BadRequest("to many rows ask the max is " + totalcount);
                 }
                 _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(Get), request);


                 var runes = await dataManager.GetItems(request.index, totalcount, request.orderingPropertyName, (request.descending == null ? false : (bool)request.descending));
                 IEnumerable<RuneDTO> res = runes.Select(c => c.ToDTO());
                 if (res == null)
                 {
                     _logger.LogWarning("No runes found with Id");
                     return BadRequest("No runes found with Id ");
                 }
                 return Ok(res);
             }
             catch (Exception e)
             {
                 _logger.LogError("About get at {e.message}", DateTime.UtcNow.ToLongTimeString());
                 return BadRequest(e.Message);

             }


         }
    
        
        [HttpGet("{name}")]
            public async Task<ActionResult<LolResponse<RuneDTO>>> GetRuneByName(string name)
            {
                try
                {

                    var rune = await dataManager
                    .GetItemsByName(name, 0, await dataManager.GetNbItems());
                    _logger.LogInformation("Executing {Action} with name : {runeName}", nameof(GetRuneByName), name);
                    RuneDTO res = rune.First().ToDTO();

                    if (res == null)
                    {
                        _logger.LogWarning("No runes found with {name}", name); ;
                        return NotFound();
                    }
                    var links = new List<EndPointLink>
                    {
                        EndPointLink.To($"/api/[controller]/{res.Name}", "self"),
                        EndPointLink.To($"/api/[controller]/{res.Name}/", "self"),
                        EndPointLink.To($"/api/[controller]/{res.Name}/", "self")
                    };

                    var response = new LolResponse<RuneDTO>(res, links);
                    return Ok(response);

                }
                catch (Exception e)
                {

                    _logger.LogError("Somthing goes wrong catching bt the Runnes controller : " + e.Message);
                    return BadRequest(e.Message);

                }
            }

    }
}
