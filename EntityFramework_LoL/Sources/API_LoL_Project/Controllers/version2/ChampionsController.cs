﻿using API_LoL_Project.Controllers.Response;
using API_LoL_Project.Middleware;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Xml.Linq;
using ApiMappeur;
using Shared;
using Microsoft.OpenApi.Extensions;

namespace API_LoL_Project.Controllers.version2
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("2.0")]
    [ApiController]
    public class ChampionsController : ControllerBase
    {
        public IChampionsManager dataManager;
        private readonly ILogger<ChampionsController> _logger;

        public ChampionsController(IDataManager dataManager, ILogger<ChampionsController> logger)
        {
            this.dataManager = dataManager.ChampionsMgr;
            _logger = logger;
        }



        // GET: api/champions/
        [HttpGet]
        public async Task<ActionResult<PageResponse<ChampionDTO>>> Get([FromQuery] Request.PageRequest request)
        {
            try
            {
                var totalcount = await dataManager.GetNbItems();
                if (request.count * request.index >= totalcount)
                {
                    _logger.LogError("To many object is asked the max is {totalcount} but the request is superior of ", totalcount);
                    return BadRequest("To many object is asked the max is : " + totalcount);
                }

                _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(Get), request); 
                
                var champions = await dataManager.GetItems(request.index, request.count == 0 ? totalcount : (int)request.count , request.orderingPropertyName, request.descending == null ? false : (bool)request.descending);
                IEnumerable<ChampionDTO> res = champions.Select(c => c.ToDTO());
                if (res.Count() <= 0 || res == null)
                {
                    _logger.LogError("No champions found the total count is {totalcount} ", totalcount);
                    return BadRequest("No champions found : totalcount is : " + totalcount);
                }

                var respList = res.Select(r => new LolResponse<ChampionDTO>
                (
                    r,
                    new List<EndPointLink>
                    {
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetChampionsImage)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetChampionsByName)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Post)}", "self","POST"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Put)}", "self","PUT"),
                    }
                ));

                var pageResponse = new PageResponse<ChampionDTO>(respList, request.index, request.count, totalcount);

                return Ok(pageResponse);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);

            }
        }


        /*// GET api/<ChampionController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ChampionFullDTO>>> GetChampionsById(int name)
        {
            try
            {

                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsByName), name);
                IEnumerable<ChampionFullDTO> res = champion.Select(c => c.toFullDTO());

                if (res == null)
                {
                    _logger.LogWarning("No chamions found with {name}", name); ;
                    return NotFound();
                }
                return Ok(res);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }*/

        // GET: api/champions/image
        [HttpGet("image/{name}")]
        public async Task<ActionResult<ImageDTO>> GetChampionsImage(string name)
        {
            try
            {
                if (name == null || name == "")
                {
                    var message = string.Format("Can not get champions image without the name (is empty)");
                    _logger.LogWarning(message + nameof(ChampionsController)); ;
                    return BadRequest(message);
                }
                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsByName), name);
                /* IEnumerable<ChampionFullDTO> res = champion.Select(c => c.toFullDTO());//.First*/
                ChampionFullDTO res = champion.First().toFullDTO();//.First

                if (res == null)
                {
                    _logger.LogWarning("No chamions found with {name}", name); ;
                    return NotFound();
                }

                return Ok(res.LargeImage);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }


        // GET api/champions/name
        [HttpGet("{name}")]
        public async Task<ActionResult<IEnumerable<LolResponse<ChampionFullDTO>>>> GetChampionsByName(string name)
        {
            if (name == null || name == "")
            {
                var message = string.Format("Can not get champions without the name (is empty)");
                _logger.LogWarning(message); ;
                return BadRequest(message);
            }
            _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsByName), name);
            try
            {
                var totalcount = await dataManager.GetNbItemsByName(name);
                if (totalcount <= 0)
                {
                    _logger.LogError("No chamions found with this name {name} in the dataContext", name); ;
                    return NotFound("No chamions found with this name: " + name + "in the dataContext");
                }
                var champion = await dataManager.GetItemsByName(name, 0, totalcount);
                _logger.LogInformation($"========================= {champion} ================================================"); ;

                if (champion.Count() <= 0 || champion == null)
                {
                    _logger.LogError($"No chamions found with {name} executing {nameof(GetChampionsByName)}"); ;
                    return NotFound("No chamions found with" + name);
                }
                IEnumerable<ChampionFullDTO> res = champion.Select(c => c.toFullDTO());
                /*ChampionFullDTO res = champion.First().toFullDTO();//.First*/
                var respList = res.Select(r => new LolResponse<ChampionFullDTO>
                (
                    r,
                    new List<EndPointLink>
                    {
                        EndPointLink.To($"/api/[controller]/{r.Name}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}", "self","POST"),
                        EndPointLink.To($"/api/[controller]/{r.Name}", "self","PUT"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/characteristic", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/characteristic", "self"),


                    }
                ));

                return Ok(respList);

            }
            catch (Exception e)
            {

                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest("Somthing goes wrong caching the Champions controller : " + e.Message);

            }
        }


        // POST api/champions/names
        // it should be better if we get a Champions From the Model but cause the Client (MAUI one) Send Champion there is no interest to be a championDTo
        /* [HttpPost]
         public async Task<IActionResult> Post([FromBody] Champion value)
         {
             try
             {
                 if (value == null)
                 {
                     var message = string.Format("Can not get champions image without the name (is empty)");
                     _logger.LogWarning(message); ;
                     return BadRequest(message);
                 }
                 // generic validation check if all field is good
                 await dataManager.AddItem(value);
                 // can we check with a method 
                 //var savedChampions = await dataManager.GetItemsByName(newChampion.name, 0, await dataManager.GetNbItems())
                 *//*if (savedChampions.Count() >= 0 || res == null)
                 {
                     _logger.LogWarning("No chamions found ");
                     return BadRequest("No chamions found ");
                 }*//*
                 _logger.LogInformation("Sucessfully saved Champions  : " + value.Name);

                 return CreatedAtAction(nameof(Get), value.ToDTO());
             }
             catch (Exception e)
             {
                 _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                 return BadRequest(e.Message);

             }

         }
 */

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ChampionFullDTO value)
        {
            try
            {

                if (value == null)
                {
                    var message = string.Format("Can not get champions image without the name (is empty)");
                    _logger.LogWarning(message); ;
                    return BadRequest(message);
                }
                /*
                 * cannot use this GetNbItemsByName and GetItemsByName use Contains and not equals 
                 * var totalcount = await dataManager.GetNbItemsByName(value.Name);
                if (totalcount >= 0)
                {
                    _logger.LogError("champions found with this name {name} in the dataContext | cannot add an champions with the same Name", value.Name); ;
                    return BadRequest("chamions this name: " + value.Name + " already exist in the dataContext champions need to have unique name");
                }*/

                // generic validation check if all field is good
                var newChampion = value.ToModel();
                await dataManager.AddItem(newChampion);
                // can we check with a method 
                var savedChampions = await dataManager.GetItemsByName(newChampion.Name, 0, await dataManager.GetNbItems());
                if (savedChampions.Count() <= 0 || savedChampions == null)
                {
                    _logger.LogWarning("No chamions found ");
                    return BadRequest("No chamions found ");
                }
                _logger.LogInformation("Sucessfully saved Champions  : " + newChampion.Name);

                return CreatedAtAction(nameof(Get), newChampion.ToDTO());
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message + e.InnerException);

            }

        }


        // should change for id cause model implementation use filteringbyName to getItemByNAme and it use substring
        // PUT api/<ChampionController>/5
        [HttpPut("{name}")]
        // should be champions full Dto
        public async Task<IActionResult> Put(string name, [FromBody] ChampionDTO value)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(Put), name);
                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                if (champion == null)
                {
                    _logger.LogError("No chamions found with {name} in the dataBase", name); ;
                    return NotFound();
                }

                var savedUpdatedChampions = await dataManager.UpdateItem(champion.First(), value.ToModel());
                if(savedUpdatedChampions == null)
                {

                        _logger.LogWarning("The updated champions not returned {name}", name); ;
                        return BadRequest();
                    
                }
                return Ok(savedUpdatedChampions);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }

        // DELETE api/<ChampionController>/5
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(Delete), name);
              
                if (string.IsNullOrEmpty(name))
                {
                    var message = string.Format("Can not delelte champions without the name (is empty)");
                    _logger.LogWarning(message + nameof(ChampionsController)); ;
                    return BadRequest(message);
                }
                var totalcount = await dataManager.GetNbItemsByName(name);
                if (totalcount <= 0)
                {
                    _logger.LogError("No chamions found with this name {name} in the dataContext cannot delete", name); 
                    return NotFound($"No chamions found with {name} cannot delete");
                }
                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());

                if (champion != null) await dataManager.DeleteItem(champion.First());
                else
                {
                    _logger.LogError($"No chamions found with {name} cannot delete"); ;
                    return NotFound($"No chamions found with {name} cannot delete");
                }
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }

        }

        /*   [HttpGet]
           public async Task<IActionResult> NbChampions()
           {
               var champions = await dataManager.GetItems(0, await dataManager.GetNbItems());
               IEnumerable<ChampionDTO> res = champions.Select(c => c.toDTO());
               return Ok(res);

           }*/

        [HttpGet("/{name}/skins")]
        public async Task<ActionResult<SkinDto>> GetChampionsSkins(string name)
        {
            _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsSkins), name);
            var champions = await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems());
            if (champions == null)
            {
                _logger.LogWarning("No chamions found with {name}", name); ;
                return NotFound();
            }
            //skinsDTO
            IEnumerable<SkinDto> res = champions.First().Skins.Select(c => c.ToDto());
            if (res == null)
            {
                _logger.LogWarning("No skins found for {name}", name); ;
                return NotFound();
            }
            return Ok(res);
        }

        /*     [HttpGet("/{name}/skills")]
             public async Task<ActionResult<SkillDto>> GetChampionsSkills(string name)
             {
                 _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsSkills), name);
                 var champions = await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems());
                 if (champions == null)
                 {
                     _logger.LogWarning("No chamions found with {name}", name); ;
                     return NotFound();
                 }
                 //skinsDTO
                 IEnumerable<SkillDto> res = champions.First().Skills.to;
                 if (res == null)
                 {
                     _logger.LogWarning("No skins found for {name}", name); ;
                     return NotFound();
                 }

                 return Ok(res);
             }*/

        [HttpPost("/{name}/skills")]
        public async Task<ActionResult<IEnumerable<SkinDto>>> AddChampionsSkills(string name)
        {
            var champions = await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems());
            //SkillDTO
            IEnumerable<Skill> res = champions.First().Skills;

            return Ok(res);
        }

        // degradation du modèle cleitn ell est dédié au client
        // GET: api/champions/Count
        [HttpGet("/count", Name = "GetChampionCount")]
        public async Task<ActionResult<int>> NbChampions()
        {
            _logger.LogInformation("method {Action} - RUNEPAGE call", nameof(NbChampions));
            try
            {
                var nbChampions = await dataManager.GetNbItems();
                return Ok(nbChampions);

            }
            catch (Exception error)
            {
                _logger.LogError(error.Message);
                return BadRequest(error.Message);

            }

        }

        [HttpGet("count/class/{championClass}")]
        public async Task<ActionResult<int>> GetChampionCountByClass(ChampionClass championClass)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(GetChampionCountByClass), championClass.GetDisplayName());

                var count = await dataManager.GetNbItemsByClass(championClass);


                return Ok(count);
            }
            catch (Exception e)
            {
                _logger.LogError("Something went wrong while fetching the count of champions by class in the controller: " + e.Message);
                return BadRequest(e.Message);
            }
        }

        [HttpGet("count/name/{substring}")]
        public async Task<ActionResult<int>> GetNbItemsByName(string substring)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with parameters: {Substring}", nameof(GetNbItemsByName), substring);

                var count = await dataManager.GetNbItemsByName(substring);

                return Ok(count);
            }
            catch (Exception e)
            {
                _logger.LogError("Something went wrong in {Action} action: {ErrorMessage}", nameof(GetNbItemsByName), e.Message);
                return BadRequest(e.Message);
            }
        }


        [HttpGet("characteristic/{characteristic}")]
        public async Task<ActionResult<PageResponse<ChampionDTO>>> GetChampionByCharacteristic(string characteristic, [FromQuery] Request.PageRequest request)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with parameters: {Parameters} ", nameof(GetChampionByCharacteristic), characteristic + " / " + request);

                var champions = await dataManager.GetItemsByCharacteristic(characteristic, request.index, request.count == 0 ? await dataManager.GetNbItems() : (int)request.count, request.orderingPropertyName, request.descending == null ? false : (bool)request.descending);

                IEnumerable<ChampionDTO> res = champions.Select(c => c.ToDTO());
                if (res.Count() <= 0 || res == null)
                {
                    _logger.LogError("No champions found for characteristic {Characteristic}", characteristic);
                    return BadRequest("No champions found for characteristic : " + characteristic);
                }

                var respList = res.Select(r => new LolResponse<ChampionDTO>
                (
                    r,
                    new List<EndPointLink>
                    {
                EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetChampionsImage)}", "self"),
                EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetChampionsByName)}", "self"),
                EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Post)}", "self","POST"),
                EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Put)}", "self","PUT"),
                    }
                ));

                var totalcount = await dataManager.GetNbItemsByCharacteristic(characteristic);
                var pageResponse = new PageResponse<ChampionDTO>(respList, request.index, request.count, totalcount);

                return Ok(pageResponse);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }




    }
}

