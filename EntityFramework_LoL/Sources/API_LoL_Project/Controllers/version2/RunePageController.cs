﻿using API_LoL_Project.Controllers.Response;
using API_LoL_Project.Middleware;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Model;
using ApiMappeur;
using API_LoL_Project.Middleware.Auth;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_LoL_Project.Controllers.version2
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("2.0")]
    [ServiceFilter(typeof(AuthMiddlewareFliter))]
    [ApiController]
    public class RunePageController : ControllerBase
    {
        private IRunePagesManager dataManager;
        private readonly ILogger<RunePageController> _logger;

        public RunePageController(IDataManager dataManager, ILogger<RunePageController> logger)
        {
            this.dataManager = dataManager.RunePagesMgr;
            _logger = logger;
        }
        // GET: api/runePage
        [HttpGet]
        public async Task<ActionResult<PageResponse<RunePageDTO>>> Get([FromQuery] Request.PageRequest request)
        {
            try
            {
                var totalcount = await dataManager.GetNbItems();
                if (request.count * request.index >= totalcount)
                {
                    _logger.LogError("To many object is asked the max is {totalcount} but the request is superior of ", totalcount);
                    return BadRequest("To many object is asked the max is : " + totalcount);
                }
                _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(Get), request);

                var runesP = await dataManager.GetItems(request.index, request.count == 0 ? totalcount : request.count, request.orderingPropertyName, request.descending == null ? false : (bool)request.descending);
                IEnumerable<RunePageDTO> res = runesP.Select(c => c.ToDto());
                if (res.Count() <= 0 || res == null)
                {
                    _logger.LogError("No RunePage found the total count is {totalcount} ", totalcount);
                    return BadRequest("No RunePage found : totalcount is : " + totalcount);
                }

                var respList = res.Select(r => new LolResponse<RunePageDTO>
                (
                    r,
                    new List<EndPointLink>
                    {
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetRunePageByName)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Post)}", "self","POST"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Put)}", "self","PUT"),
                    }
                ));

                var pageResponse = new PageResponse<RunePageDTO>(respList, request.index, request.count, totalcount);

                return Ok(pageResponse);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the RunePage controller : " + e.Message);
                return BadRequest(e.Message);

            }
        }
        // GET: api/<RunePageController>
        [HttpGet("{name}")]
        public async Task<ActionResult<IEnumerable<LolResponse<RunePageDTO>>>> GetRunePageByName(string name)
        {
            if (name == null || name == "")
            {
                var message = string.Format("Can not get Rune PAge without the name (is empty)");
                _logger.LogWarning(message); ;
                return BadRequest(message);
            }
            _logger.LogInformation("Executing {Action} with name : {runePageName}", nameof(GetRunePageByName), name);
            try
            {
                var totalcount = await dataManager.GetNbItemsByName(name);
                if (totalcount <= 0)
                {
                    _logger.LogError("No RunePage found with this name {name} in the dataContext", name); ;
                    return NotFound("No RunePage found with this name: " + name + "in the dataContext");
                }
                var runeP = await dataManager.GetItemsByName(name, 0, totalcount);
                _logger.LogInformation($"========================= {runeP} ================================================"); ;

                if (runeP.Count() <= 0 || runeP == null)
                {
                    _logger.LogError($"No RunePage found with {name} executing {nameof(GetRunePageByName)}"); ;
                    return NotFound("No RunePage found with" + name);
                }
                IEnumerable<RunePageDTO> res = runeP.Select(c => c.ToDto());
                var respList = res.Select(r => new LolResponse<RunePageDTO>
                (
                    r,
                    new List<EndPointLink>
                    {
                      EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetRunePageByName)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Post)}", "self","POST"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Put)}", "self","PUT"),
                    }
                ));

                return Ok(respList);

            }
            catch (Exception e)
            {

                _logger.LogError("Somthing goes wrong caching the RunePage controller : " + e.Message);
                return BadRequest("Somthing goes wrong caching the RunePage controller : " + e.Message);

            }
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RunePageDTO runePage)
        {
            _logger.LogInformation("method {Action} - RunPageController with {item}", nameof(Post), runePage);
            try
            {
                if (await dataManager.GetNbItemsByName(runePage.Name) == 0)
                {
                    return CreatedAtAction(nameof(Get),(await dataManager.AddItem(runePage.ToModel())).ToDto());
                }
                _logger.LogWarning($"Name : {runePage.Name} already exist");
                return BadRequest($"Name : {runePage.Name} already exist");
            }
            catch (Exception error)
            {
                _logger.LogError(error.Message);
                return BadRequest(error.Message);
            }
        }

        [HttpPut("{name}")]
        public async Task<IActionResult> Put(string name, [FromBody] RunePageDTO value)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {RunePageName}", nameof(Put), name);
                var runeP = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                if (runeP == null)
                {
                    _logger.LogError("No RunePage found with {name} in the dataBase", name); ;
                    return NotFound();
                }

                var savedUpdatedRunePage = await dataManager.UpdateItem(runeP.First(), value.ToModel());
                if (savedUpdatedRunePage == null)
                {

                    _logger.LogWarning("The updated RunePage not returned {name}", name); ;
                    return BadRequest();

                }
                return Ok(savedUpdatedRunePage);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the RunePage controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }


        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            _logger.LogInformation("method {Action} - RUNEPAGE call with {name}", nameof(Delete), name);
            try
            {
                var dtos = await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems());
                if (dtos == null)
                {
                    _logger.LogWarning("{name} was not found", name);
                    return NotFound($"{name} was not found");
                }
                await dataManager.DeleteItem(dtos.First());
                return NoContent();
            }
            catch (Exception error)
            {
                _logger.LogError(error.Message);
                return BadRequest(error.Message);
            }
        }
        // GET: api/<RunePageController>/count
        [HttpGet("/countRunePage", Name = "GetRunePageCount") ]
        public async Task<ActionResult<int>> NbRunePage()
        {
            _logger.LogInformation("method {Action} - RUNEPAGE call", nameof(NbRunePage));
            try
            {
                var nbRunePage = await dataManager.GetNbItems();
                return Ok(nbRunePage);

            }
            catch (Exception error)
            {
                _logger.LogError(error.Message);
                return BadRequest(error.Message);

            }

        }


    }
}
