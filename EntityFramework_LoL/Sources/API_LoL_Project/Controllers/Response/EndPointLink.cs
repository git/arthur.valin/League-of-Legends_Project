﻿namespace API_LoL_Project.Controllers.Response
{
    public class EndPointLink
    {
        public string Href { get; set; }
        public string Rel { get; set; }
        public string Method { get; set; }
        public EndPointLink()
        {
        }
        public EndPointLink(string href, string rel, string method)
        {
            Href = href;
            Rel = rel;
            Method = method;
        }
        public static EndPointLink To(string href, string rel = "self", string method = "GET")
        {
            return new EndPointLink { Href = href, Rel = rel, Method = method };
        }
    }
}
