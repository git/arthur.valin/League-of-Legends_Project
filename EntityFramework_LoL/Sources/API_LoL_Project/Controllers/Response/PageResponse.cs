﻿using API_LoL_Project.Middleware;

namespace API_LoL_Project.Controllers.Response
{
    public class PageResponse<T>
    {
        public IEnumerable<LolResponse<T>> Data { get; set; }
        public int Index { get; set; } = 1;
        public int TotalCount { get; set; } = 1;
        public int Count { get; set; } = 1;


        public PageResponse(IEnumerable<LolResponse<T>> data, int indexRequested, int countRequested, int totalCount)
        {
            this.Data = data;
            this.Index = indexRequested;
            this.TotalCount = totalCount;
            this.Count = countRequested;
        }
    }
}

