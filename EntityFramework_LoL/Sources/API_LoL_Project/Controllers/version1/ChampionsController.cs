﻿using API_LoL_Project.Controllers.Response;
using API_LoL_Project.Middleware;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using System.Collections.Generic;
using System.Text.Json;
using System.Xml.Linq;
using ApiMappeur;

namespace API_LoL_Project.Controllers.version1
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    [ApiController]
    public class ChampionsController : ControllerBase
    {
        public IChampionsManager dataManager;
        private readonly ILogger<ChampionsController> _logger;
        public ChampionsController(IDataManager dataManager, ILogger<ChampionsController> logger)
        {
            this.dataManager = dataManager.ChampionsMgr;
            _logger = logger;
        }

        // GET: api/<ChampionController>/getAllChampions
        [HttpGet]
        public async Task<ActionResult<PageResponse<ChampionDTO>>> Get([FromQuery] Request.PageRequest request)
        {
            try
            {
                var totalcount = await dataManager.GetNbItems();
                if (request.count * request.index >= totalcount)
                {
                    _logger.LogError("To many object is asked the max is {totalcount} but the request is superior of ", totalcount);
                    return BadRequest("To many object is asked the max is : " + totalcount);
                }

                _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(Get), request); ;
                var champions = await dataManager.GetItems(request.index, request.count, request.orderingPropertyName, request.descending == null ? false : (bool)request.descending);
                IEnumerable<ChampionDTO> res = champions.Select(c => c.ToDTO());
                if (res.Count() <= 0 || res == null)
                {
                    _logger.LogError("No champions found the total count is {totalcount} ", totalcount);
                    return BadRequest("No champions found : totalcount is : " + totalcount);
                }

                var respList = res.Select(r => new LolResponse<ChampionDTO>
                (
                    r,
                    new List<EndPointLink>
                    {
                        EndPointLink.To($"/api1111111111/[controller]/{r.Name}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Get)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetChampionsImage)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(GetChampionsByName)}", "self"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Post)}", "self","POST"),
                        EndPointLink.To($"/api/[controller]/{r.Name}/{nameof(Put)}", "self","PUT"),
                    }
                ));

                var pageResponse = new PageResponse<ChampionDTO>(respList, request.index, request.count, totalcount);

                return Ok(pageResponse);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);

            }
        }


        /*// GET api/<ChampionController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<ChampionFullDTO>>> GetChampionsById(int name)
        {
            try
            {

                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsByName), name);
                IEnumerable<ChampionFullDTO> res = champion.Select(c => c.toFullDTO());

                if (res == null)
                {
                    _logger.LogWarning("No chamions found with {name}", name); ;
                    return NotFound();
                }
                return Ok(res);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }*/

        // GET: api/<ChampionController>/LargeImage
        [HttpGet("image/{name}")]
        public async Task<ActionResult<ImageDTO>> GetChampionsImage(string name)
        {
            try
            {
                if (name == null || name == "")
                {
                    var message = string.Format("Can not get champions image without the name (is empty)");
                    _logger.LogWarning(message); ;
                    return BadRequest(message);
                }
                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsByName), name);
                /* IEnumerable<ChampionFullDTO> res = champion.Select(c => c.toFullDTO());//.First*/
                ChampionFullDTO res = champion.First().toFullDTO();//.First

                if (res == null)
                {
                    _logger.LogWarning("No chamions found with {name}", name); ;
                    return NotFound();
                }

                return Ok(res.LargeImage);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }


        // GET api/<ChampionController>/name
        [HttpGet("{name}")]
        public async Task<ActionResult<LolResponse<ChampionFullDTO>>> GetChampionsByName(string name)
        {
            if (name == null || name == "")
            {
                var message = string.Format("Can not get champions without the name (is empty)");
                _logger.LogWarning(message); ;
                return BadRequest(message);
            }
            _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsByName), name);
            try
            {
                var totalcount = await dataManager.GetNbItemsByName(name);
                if (totalcount <= 0)
                {
                    _logger.LogError("No chamions found with this name {name} in the dataContext", name); ;
                    return BadRequest("No chamions found with this name: " + name + "in the dataContext");
                }

                var champion = await dataManager.GetItemsByName(name, 0, totalcount);
                /* IEnumerable<ChampionFullDTO> res = champion.Select(c => c.toFullDTO());//.First*/
                if (champion.Count() <= 0 || champion == null)
                {
                    _logger.LogError($"No chamions found with {name} executing {nameof(GetChampionsByName)}"); ;
                    return NotFound("No chamions found with" + name);
                }
                ChampionFullDTO res = champion.First().toFullDTO();//.First


                var links = new List<EndPointLink>
                {
                    EndPointLink.To($"/api/[controller]/{res.Name}", "self"),
                    EndPointLink.To($"/api/[controller]/{res.Name}/", "self"),
                    EndPointLink.To($"/api/[controller]/{res.Name}/", "self")
                };

                var response = new LolResponse<ChampionFullDTO>(res, links);
                return Ok(response);

            }
            catch (Exception e)
            {

                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest("Somthing goes wrong caching the Champions controller : " + e.Message);

            }
        }


        // POST api/<Champ
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ChampionFullDTO value)
        {
            try
            {
                // generic validation
                var newChampion = value.ToModel();
                await dataManager.AddItem(newChampion);
                // can we check with a method 
                //var savedChampions = await dataManager.GetItemsByName(newChampion.name, 0, await dataManager.GetNbItems())
                /*if (savedChampions.Count() >= 0 || res == null)
                {
                    _logger.LogWarning("No chamions found ");
                    return BadRequest("No chamions found ");
                }*/
                _logger.LogInformation("Sucessfully saved Champions  : " + newChampion.Name);

                return CreatedAtAction(nameof(Get), newChampion);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);

            }

        }
        // should change for id cause model implementation use filteringbyName to getItemByNAme and it use substring
        // PUT api/<ChampionController>/5
        [HttpPut("{name}")]
        public async Task<IActionResult> Put(string name, [FromBody] ChampionDTO value)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(Put), name);
                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                if (champion == null)
                {
                    _logger.LogError("No chamions found with {name} in the dataBase", name); ;
                    return NotFound();
                }

                await dataManager.UpdateItem(champion.First(), value.ToModel());
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }

        // DELETE api/<ChampionController>/5
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(Delete), name);
                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());

                if (champion != null) await dataManager.DeleteItem(champion.First());
                else
                {
                    _logger.LogError($"No chamions found with {name} cannot delete"); ;
                    return NotFound($"No chamions found with {name} cannot delete");
                }
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }

        }

        /*   [HttpGet]
           public async Task<IActionResult> NbChampions()
           {
               var champions = await dataManager.GetItems(0, await dataManager.GetNbItems());
               IEnumerable<ChampionDTO> res = champions.Select(c => c.toDTO());
               return Ok(res);

           }*/

        [HttpGet("/{name}/skins")]
        public async Task<ActionResult<SkinDto>> GetChampionsSkins(string name)
        {
            _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsSkins), name);
            var champions = await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems());
            if (champions == null)
            {
                _logger.LogWarning("No chamions found with {name}", name); ;
                return NotFound();
            }
            //skinsDTO
            IEnumerable<SkinDto> res = champions.First().Skins.Select(c => c.ToDto());
            if (res == null)
            {
                _logger.LogWarning("No skins found for {name}", name); ;
                return NotFound();
            }
            return Ok(res);
        }

        /*     [HttpGet("/{name}/skills")]
             public async Task<ActionResult<SkillDto>> GetChampionsSkills(string name)
             {
                 _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampionsSkills), name);
                 var champions = await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems());
                 if (champions == null)
                 {
                     _logger.LogWarning("No chamions found with {name}", name); ;
                     return NotFound();
                 }
                 //skinsDTO
                 IEnumerable<SkillDto> res = champions.First().Skills.to;
                 if (res == null)
                 {
                     _logger.LogWarning("No skins found for {name}", name); ;
                     return NotFound();
                 }

                 return Ok(res);
             }*/

        [HttpPost("/{name}/skills")]
        public async Task<ActionResult<IEnumerable<SkinDto>>> AddChampionsSkills(string name)
        {
            var champions = await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems());
            //SkillDTO
            IEnumerable<Skill> res = champions.First().Skills;

            return Ok(res);
        }

        // degradation du modèle cleitn ell est dédié au cliens
        // GET: api/champions/Count

        [HttpGet("/count", Name = "GetChampionCount")]
        public async Task<ActionResult<int>> NbChampions()
        {
            var nbChampions = await dataManager.GetNbItems();
            return Ok(nbChampions);
        }

    }
}

