﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Model;

namespace Business
{
    public partial class DbData : IDataManager
    {
        public DbData(LolDbContext dbContext)
        {
            DbContext = dbContext;
            ChampionsMgr = new ChampionsManager(this);
            SkinsMgr = new SkinsManager(this);
            RunesMgr = new RunesManager(this);
            RunePagesMgr = new RunePagesManager(this);
        }

        protected LolDbContext DbContext{ get; }

        public IChampionsManager ChampionsMgr { get; }

        public ISkinsManager SkinsMgr { get; }

        public IRunesManager RunesMgr { get; }

        public IRunePagesManager RunePagesMgr { get; }
    }
}