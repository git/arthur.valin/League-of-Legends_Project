﻿using EntityMapper;
using Microsoft.EntityFrameworkCore;
using Model;

namespace Business
{
    public partial class DbData
    {
        public class SkinsManager : ISkinsManager
        {
            private readonly DbData parent;

            public SkinsManager(DbData parent)
                => this.parent = parent;

            public async Task<Skin?> AddItem(Skin? item)
            {
                await parent.DbContext.skins.AddAsync(item.ToEntity(parent.DbContext));
                parent.DbContext.SaveChanges();
                return item;
            }

            public async Task<bool> DeleteItem(Skin? item)
            {
                var toDelete = parent.DbContext.skins.Find(item.Name);
                parent.DbContext.skins.Remove(toDelete);
                parent.DbContext.SaveChanges();
                return true;
            }

            public async Task<IEnumerable<Skin?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.skins.Include("Champion").GetItemsWithFilterAndOrdering(
                    s => true,
                    index, count,
                    orderingPropertyName, descending).Select(s => s?.ToModel());
            }

            public async Task<IEnumerable<Skin?>> GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.skins.Include("Champion").GetItemsWithFilterAndOrdering(
                    s => s.Champion.Name.Equals(champion?.Name),
                    index, count,
                    orderingPropertyName, descending).Select(s => s?.ToModel());
            }

            public async Task<IEnumerable<Skin?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.skins.Include("Champion").GetItemsWithFilterAndOrdering(
                    s => s.Name.Contains(substring),
                    index, count,
                    orderingPropertyName, descending).Select(s => s.ToModel());
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.skins.Count();
            }

            public async Task<int> GetNbItemsByChampion(Champion? champion)
            {
                if (champion != null)
                {
                    return parent.DbContext.skins.Where(s => s.Champion.Name.Equals(champion.Name))
                    .Count();
                }
                return 0;
            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                return parent.DbContext.skins.Where(s => s.Name.Contains(substring))
                .Count();
            }

            public async Task<Skin?> UpdateItem(Skin? oldItem, Skin? newItem)
            {
                var toUpdate = parent.DbContext.skins.Find(oldItem.Name);
                toUpdate.Champion = parent.DbContext.champions.Find(newItem.Champion.Name);
                return newItem;
            }
        }

    }
}
