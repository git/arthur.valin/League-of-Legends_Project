﻿using EntityMapper;
using Microsoft.EntityFrameworkCore;
using Model;
using Shared;
using System.Diagnostics;

namespace Business
{
    public partial class DbData
    {
        public class ChampionsManager : IChampionsManager
        {
            private readonly DbData parent;

            public ChampionsManager(DbData parent)
                => this.parent = parent;

            public async Task<Champion?> AddItem(Champion? item)
            {
                await parent.DbContext.champions.AddAsync(item.ToEntity(parent.DbContext));
                parent.DbContext.SaveChanges();
                return item;
            }

            public async Task<bool> DeleteItem(Champion? item)
            {
                var toDelete = parent.DbContext.champions.Find(item.Name);
                if (toDelete!=null)
                {
                    parent.DbContext.champions.Remove(toDelete);
                    parent.DbContext.SaveChanges();
                    return true;
                }
                return false;
            }

            public async Task<IEnumerable<Champion?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                Console.WriteLine("GET");
                return parent.DbContext.champions.Include("Skills").Include("Characteristics").GetItemsWithFilterAndOrdering(
                        c => true,
                        index, count,
                        orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsByCharacteristic(string charName, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {

                return parent.DbContext.champions.Include("Skills").Include("Characteristics").GetItemsWithFilterAndOrdering(
                    c => c.Characteristics.Any(ch => ch.Name.Equals(charName)),
                    index, count,
                    orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsByClass(ChampionClass championClass, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.champions.Include("Skills").Include("Characteristics").GetItemsWithFilterAndOrdering(
                    c => c.Class.Equals(championClass),
                    index, count,
                    orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.champions.Include("Skills").Include("Characteristics").GetItemsWithFilterAndOrdering(
                        c => c.Name.Contains(substring),
                        index, count,
                        orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsByRunePage(RunePage? runePage, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {

                return parent.DbContext.champions.Include("Skills").Include("Characteristics").Include("runepages").GetItemsWithFilterAndOrdering(
                        c => c.runepages.Any(rp => rp.Equals(runePage.ToEntity(parent.DbContext))),
                        index, count,
                        orderingPropertyName, descending).Select(c => c.ToModel());

            }

            public async Task<IEnumerable<Champion?>> GetItemsBySkill(Skill? skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.champions.Include("Skills").Include("Characteristics").GetItemsWithFilterAndOrdering(
                        c => skill != null && c.Skills.Any(s => s.Name.Equals(skill.Name)),
                        index, count,
                        orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsBySkill(string skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.champions.Include("Skills").Include("Characteristics").Include("Skills").GetItemsWithFilterAndOrdering(
                    c => skill != null && c.Skills.Any(s => s.Name.Equals(skill)),
                    index, count,
                    orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.champions.Count();
            }

            public async Task<int> GetNbItemsByCharacteristic(string charName)
            {
                return parent.DbContext.champions.Where(c => c.Characteristics.Any(ch => ch.Name.Equals(charName))).Count();
            }

            public async Task<int> GetNbItemsByClass(ChampionClass championClass)
            {
                return parent.DbContext.champions.Where(c => c.Class.Equals(championClass))
                .Count();
            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                return parent.DbContext.champions.Where(c => c.Name.Contains(substring))
                .Count();
            }

            public async Task<int> GetNbItemsByRunePage(RunePage? runePage)
            {
                return parent.DbContext.champions.Where(c => c.runepages.Any(rp => rp.Equals(runePage.ToEntity(parent.DbContext)))).Count();

            }

            public async Task<int> GetNbItemsBySkill(Skill? skill)
            {
                return parent.DbContext.champions.Where(c => skill != null && c.Skills.Any(s => s.Name.Equals(skill.Name)))
                    .Count();
            }

            public async Task<int> GetNbItemsBySkill(string skill)
            {
                return parent.DbContext.champions.Where(c => skill != null && c.Skills.Any(s => s.Name.Equals(skill)))
                    .Count();
            }

            public async Task<Champion?> UpdateItem(Champion? oldItem, Champion? newItem)
            {
                var toUpdate = parent.DbContext.champions.Find(oldItem.Name);
                toUpdate = newItem.ToEntity(parent.DbContext);
                parent.DbContext.SaveChanges();
                return newItem;
            }
        }
    }
}
