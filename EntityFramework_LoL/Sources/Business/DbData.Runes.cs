﻿using EntityMapper;
using Microsoft.EntityFrameworkCore;
using Model;
using Shared;

namespace Business
{
    public partial class DbData
    {
        public class RunesManager : IRunesManager
        {
            private readonly DbData parent;

            public RunesManager(DbData parent)
                => this.parent = parent;
            public async Task<Rune?> AddItem(Rune? item)
            {
                await parent.DbContext.runes.AddAsync(item.ToEntity(parent.DbContext));
                parent.DbContext.SaveChanges();
                return item;
            }

            public async Task<bool> DeleteItem(Rune? item)
            {
                var toDelete = parent.DbContext.runes.Find(item?.Name);
                if (toDelete != null)
                {
                    parent.DbContext.runes.Remove(item?.ToEntity(parent.DbContext));
                    parent.DbContext.SaveChanges();
                    return true;
                }
                return false;
            }

            public async Task<IEnumerable<Rune?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.runes.GetItemsWithFilterAndOrdering(
                        r => true,
                        index, count,
                        orderingPropertyName, descending).Select(r => r.ToModel());
            }

            public async Task<IEnumerable<Rune?>> GetItemsByFamily(RuneFamily family, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.runes.GetItemsWithFilterAndOrdering(
                    r => r.RuneFamily.Equals(family),
                    index, count,
                    orderingPropertyName, descending).Select(r => r.ToModel());
            }

            public async Task<IEnumerable<Rune?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.runes.GetItemsWithFilterAndOrdering(
                    r => r.Name.Contains(substring),
                    index, count,
                    orderingPropertyName, descending).Select(r => r.ToModel());
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.runes.Count();
            }

            public async Task<int> GetNbItemsByFamily(RuneFamily family)
            {
                return parent.DbContext.runes.Where(r => r.RuneFamily.Equals(family)).Count();
            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                return parent.DbContext.runes.Where(r => r.Name.Contains(substring)).Count();
            }

            public async Task<Rune?> UpdateItem(Rune? oldItem, Rune? newItem)
            {
                var toUpdate = parent.DbContext.runes.Find(oldItem.Name);
                    toUpdate.Description = newItem.Description;
                    toUpdate.RuneFamily = newItem.Family;
                    parent.DbContext.SaveChanges();

                return newItem;
            }
        }
    }
}
