﻿using Entities;
using EntityMapper;
using Microsoft.EntityFrameworkCore;
using Model;

namespace Business
{
    public partial class DbData
    {
        public class RunePagesManager : IRunePagesManager
        {
            private readonly DbData parent;

            public RunePagesManager(DbData parent)
                => this.parent = parent;

            public async Task<RunePage?> AddItem(RunePage? item)
            {
                await parent.DbContext.runepages.AddAsync(item.ToEntity(parent.DbContext));
                parent.DbContext.SaveChanges();
                return item;
            }

            public async Task<bool> DeleteItem(RunePage? item)
            {
                var toDelete = parent.DbContext.runepages.Find(item.Name);
                if (toDelete != null)
                {
                    parent.DbContext.runepages.Remove(toDelete);
                    parent.DbContext.SaveChanges();
                    return true;
                }
                return false;
            }

            public async Task<IEnumerable<RunePage?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.runepages.Include("entries").GetItemsWithFilterAndOrdering(
                        rp => true,
                        index, count,
                        orderingPropertyName, descending).Select(rp => rp.ToModel(parent.DbContext));
            }

            public async Task<IEnumerable<RunePage?>> GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.runepages.Include("champions").GetItemsWithFilterAndOrdering(
                    rp => rp.champions.Any(c => c.Name.Equals(champion.Name)),
                    index, count,
                    orderingPropertyName, descending).Select(rp => rp.ToModel(parent.DbContext));
            }

            public async Task<IEnumerable<RunePage?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.runepages.Include("entries").GetItemsWithFilterAndOrdering(
                    rp => rp.Name.Contains(substring),
                    index, count,
                    orderingPropertyName, descending).Select(rp => rp.ToModel(parent.DbContext));
            }

            public async Task<IEnumerable<RunePage?>> GetItemsByRune(Rune? rune, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.runepages.Include("entries").GetItemsWithFilterAndOrdering(
                    rp => rp.entries.Any(r => r.RuneName.Equals(rune.Name)),
                    index, count,
                    orderingPropertyName, descending).Select(rp => rp.ToModel(parent.DbContext));
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.runepages.Count();

            }

            public async Task<int> GetNbItemsByChampion(Champion? champion)
            {
                return parent.DbContext.runepages.Where(rp => rp.champions.Any(c => c.Name.Equals(champion.Name))).Count();

            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                return parent.DbContext.runepages.Where(rp => rp.Name.Contains(substring)).Count();
            }

            public async Task<int> GetNbItemsByRune(Model.Rune? rune)
            {
                return parent.DbContext.runepages.Where(rp => rp.entries.Any(r => r.RuneName.Equals(rune.Name))).Count();
            }

            public async Task<RunePage?> UpdateItem(RunePage? oldItem, RunePage? newItem)
            {
                var toUpdate = parent.DbContext.runepages.Include("entries")
                    .Where(x => x.Name.Equals(newItem.Name)).First();
                    toUpdate.entries = newItem.Runes.Select(r => new RunePageRuneEntity()
                                                                {
                                                                    category = r.Key,
                                                                    rune = r.Value.ToEntity(parent.DbContext),
                                                                }).ToList();

                    parent.DbContext.SaveChanges();

                return newItem;

            }
        }
    }
}
