﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class RunePageEntity
    {
        [Key]
        [MaxLength(256)]
        public string Name { get; set; }

        public ICollection<RunePageRuneEntity> entries { get; set; }
        public ICollection<ChampionEntity> champions { get; set; }
    }
}
