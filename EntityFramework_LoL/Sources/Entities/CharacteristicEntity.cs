﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class CharacteristicEntity
    {
        [Key]
        [MaxLength(256)]
        public string Name { get; set; }

        [Required]
        public int Value { get; set; }

        [Required]
        public string ChampionForeignKey { get; set; }

        [ForeignKey("ChampionForeignKey")]
        public ChampionEntity Champion { get; set; }
    }
}