﻿using Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class RuneEntity
    {
        [Key]
        [MaxLength(256)]
        public string Name { get; set; }

        [Required]
        [MaxLength(500)]
        public string Description { get; set; }

        public string Icon { get; set; }

        [Required]
        public RuneFamily RuneFamily { get; set; }
        public ICollection<RunePageRuneEntity>? runepages { get; set; }

        public Guid? ImageId { get; set; }

        [ForeignKey("ImageId")]
        public LargeImageEntity? Image { get; set; }

    }
}
