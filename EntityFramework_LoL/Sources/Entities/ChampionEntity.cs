﻿using Shared;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public class ChampionEntity
    {
        [Key]
        [MaxLength(256)]
        public string Name { get; set; }
        [Required]
        [MaxLength(500)]
        public string Bio { get; set; }
        public string Icon { get; set; }

        [Required]
        public ChampionClass Class { get; set;}
        public virtual ICollection<SkillEntity> Skills { get; set; }
        public virtual ICollection<CharacteristicEntity> Characteristics { get; set; }
        public ICollection<RunePageEntity> runepages { get; set; }

        public Guid? ImageId { get; set; }
        [ForeignKey("ImageId")]
        public LargeImageEntity? Image { get; set; }
    }
}
