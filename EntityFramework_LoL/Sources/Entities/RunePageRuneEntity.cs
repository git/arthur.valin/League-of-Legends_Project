﻿using Shared;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public class RunePageRuneEntity
    {
        public Category category { get; set; }

        [ForeignKey("RuneName")]
        public RuneEntity rune { get; set; }
        public string RuneName { get; set; }

        [ForeignKey("RunePageName")]
        public RunePageEntity runepage { get; set; }
        public string RunePageName { get; set; }

    }
}
