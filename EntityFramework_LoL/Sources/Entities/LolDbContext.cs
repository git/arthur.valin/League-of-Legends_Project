﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Shared;
using System.Reflection.Metadata;
using System.Security.Claims;
using System.Xml.Linq;

namespace Entities
{
    public class LolDbContext : DbContext
    {
        public DbSet<SkinEntity> skins { get; set; }
        public DbSet<ChampionEntity> champions { get; set; }
        public DbSet<CharacteristicEntity> characteristics { get; set; }
        public DbSet<SkillEntity> skills { get; set; }
        public DbSet<RuneEntity> runes { get; set; }
        public DbSet<RunePageEntity> runepages { get; set; }
        public DbSet<LargeImageEntity> largeimages { get; set; }
        public DbSet<RunePageRuneEntity> runepagerunes { get; set; }
        public LolDbContext(DbContextOptions configuration) : base(configuration){}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured) {
                Console.WriteLine("!IsConfigured...");
                optionsBuilder.UseSqlite($"Data Source=Entities.Champions.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<LargeImageEntity>().Property(e => e.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<CharacteristicEntity>().HasKey(c => new { c.Name, c.ChampionForeignKey });
            modelBuilder.Entity<RunePageRuneEntity>().HasKey(c => new { c.RunePageName, c.RuneName });

            //modelBuilder.Entity<RunePageEntity>().Property(e => e.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<ChampionEntity>()
            .HasMany(x => x.runepages)
            .WithMany(x => x.champions);
        }
    }

    internal record NewRecord(string Name, string Item);
}
