﻿using Microsoft.EntityFrameworkCore;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    internal class LolDbContextWithStub : LolDbContext
    {
        public LolDbContextWithStub(DbContextOptions configuration) : base(configuration)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<LargeImageEntity>().HasData(new List<LargeImageEntity>()
            {
                new()
                {
                    Id = Guid.NewGuid(),
                    Base64 = "aaa"
                }
            });

            modelBuilder.Entity<ChampionEntity>().HasData(new List<ChampionEntity>() {
                new()
                {
                    Name = "Dave",
                    Bio = "Le meilleur Jazzman de France",
                    Class = ChampionClass.Fighter,

                },
                new()
                {
                    Name = "Armure",
                    Bio = "Solide",
                    Class = ChampionClass.Tank,
                }
            });

            modelBuilder.Entity<CharacteristicEntity>().HasData(new List<CharacteristicEntity>() {
                new()
                {
                    Name = "Force",
                    Value = 50,
                    ChampionForeignKey = "Dave",
                },
                new()
                {
                    Name = "Défense",
                    Value = 75,
                    ChampionForeignKey = "Armure",
                }
            });

            modelBuilder.Entity<SkinEntity>().HasData(new List<SkinEntity>() {
                new SkinEntity
                {
                    Name = "Dave de glace",
                    Description = "Enneigé",
                    Icon = "aaa",
                    ChampionForeignKey = "Dave",
                    Price=7.99F
                },
                new SkinEntity
                {
                    Name = "Armure Fullspeed",
                    Description = "Deja vu",
                    Icon = "aaa",
                    ChampionForeignKey = "Armure",
                    Price=9.99F
                },
            });

            modelBuilder.Entity<SkillEntity>().HasData(new List<SkillEntity>() {
                new()
                {
                    Name = "Boule de feu",
                    Description = "Fire!",
                    SkillType = SkillType.Basic
                },
                new()
                {
                    Name = "White Star",
                    Description = "Random damage",
                    SkillType = SkillType.Ultimate
                }
            });

            modelBuilder.Entity<RunePageEntity>().HasData(new List<RunePageEntity>()
            {
                new()
                {
                    //Id = Guid.NewGuid(),
                    Name="Runepage_1"
                }
            });

            modelBuilder.Entity<RuneEntity>().HasData(new List<RuneEntity>() {
                new()
                {
                    Name = "Bullseye",
                    Description = "Steady shot",
                    RuneFamily = RuneFamily.Precision
                },
                new()
                {
                    Name = "Alkatraz",
                    Description = "Lock effect",
                    RuneFamily = RuneFamily.Domination
                }
            });

        }
    }
}
