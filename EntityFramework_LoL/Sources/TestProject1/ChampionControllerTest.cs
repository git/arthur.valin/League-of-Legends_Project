using API_LoL_Project.Controllers;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;

namespace TestProject1
{
    [TestClass]
    public class ChampionControllerTest
    {
        public readonly ChampionsController championCtrl;

        public readonly IChampionsManager stubMgr;
        public ChampionControllerTest()
        {
            stubMgr = new StubData().ChampionsMgr;

            championCtrl = new ChampionsController(stubMgr);
        }

        [TestMethod]
        public async Task TestGetChampions()
        {
            var getResult = await championCtrl.Get();

            var objectRes = getResult as OkObjectResult;

            Assert.IsNotNull(objectRes);

            var champions = objectRes?.Value as IEnumerable<ChampionDTO>;
            Assert.IsNotNull(champions);

            Assert.AreEqual(champions.Count(), await stubMgr.GetNbItems());

        }


        [TestMethod]
        public async void TestPostChampions()
        {
            var chamionGetResul = championCtrl.Get();

        }

        [TestMethod]
        public async void TestUpdateChampions()
        {
            var chamionGetResul = championCtrl.Get();

        }


        [TestMethod]
        public async void TestDeleteChampions()
        {
            var chamionGetResul = championCtrl.Get();

        }

    }
}