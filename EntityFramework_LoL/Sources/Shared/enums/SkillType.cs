﻿namespace Shared
{
	public enum SkillType
	{
		Unknown,
		Basic,
		Passive,
		Ultimate
	}
}

