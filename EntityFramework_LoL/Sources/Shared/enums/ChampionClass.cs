﻿namespace Shared
{
	public enum ChampionClass
	{
		Unknown,
		Assassin,
		Fighter,
		Mage,
		Marksman,
		Support,
		Tank,
	}
}

