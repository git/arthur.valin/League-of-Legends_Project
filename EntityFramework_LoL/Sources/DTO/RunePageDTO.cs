﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class RunePageDTO
    {
        public string Name { get; set; }
        public Dictionary<string, RuneDTO> Runes { get; set; }
    }
}
