﻿using Model;
using Shared;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using static System.Net.Mime.MediaTypeNames;

namespace DTO
{
    public class ChampionDTO
    {
        public string Name { get; set; }
        public string Bio { get; set; }
        public string Icon { get; set; }
        public ChampionClass Class { get; set; }
    }

    public class ChampionFullDTO
    {

        [JsonPropertyName("characteristics")]
        public ReadOnlyDictionary<string, int> Characteristics { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("bio")]
        public string Bio { get; set; }

        [JsonPropertyName("class")]
        public ChampionClass Class { get; set; }

        [JsonPropertyName("icon")]
        public string Icon { get; set; }


        [JsonPropertyName("largeImage")]
        public ImageDTO LargeImage { get; set; }

        [JsonPropertyName("skins")]
        public IEnumerable<SkinDto> Skins { get; set; }

        [JsonPropertyName("skills")]
        public IEnumerable<Skill> Skills { get; set; }
    }

}