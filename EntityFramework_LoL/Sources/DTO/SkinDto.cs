﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SkinDto
    {
/*        public string Champion { get; set; }
*/        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public float Price { get; set; }
        public ImageDTO LargeImage { get; set; }


    }
    public class SkinFullDto
    {
        public string Champion { get; set; }
        
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public float Price { get; set; }
        public ImageDTO LargeImage { get; set; }


    }
}
