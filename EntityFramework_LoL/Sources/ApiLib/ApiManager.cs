﻿using Model;
using System.Net.Http;

namespace ApiLib
{
    public partial class ApiManager : IDataManager
    {
        protected HttpClient HttpClient { get; }


        public ApiManager(HttpClient httpClient)
        {
            ChampionsMgr = new ChampionsManager(this);
            SkinsMgr = new SkinsManager(this);
            /*RunesMgr = new RunesManager(this);
            RunePagesMgr = new RunePagesManager(this);*/

            HttpClient = httpClient;

            HttpClient.BaseAddress = new Uri("https://codefirst.iut.uca.fr/containers/arthurvalin-lolcontainer/api/v2");

        }

        public IChampionsManager ChampionsMgr { get; }

        public ISkinsManager SkinsMgr { get; }

        public IRunesManager RunesMgr { get; }

        public IRunePagesManager RunePagesMgr { get; }
    }
}