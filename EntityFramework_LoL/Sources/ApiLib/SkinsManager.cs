﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DTO;
using ApiMappeur;

namespace ApiLib
{
    public partial class ApiManager
    {
        public class SkinsManager : ISkinsManager
        {
            private readonly ApiManager parent;

            public SkinsManager(ApiManager parent)
                => this.parent = parent;

            public async Task<Skin?> AddItem(Skin? item)
            {
                try
                {
                    if (item == null) throw new ArgumentNullException("Skin is null cannot add empty");

                    var response = await parent.HttpClient.PostAsJsonAsync("/skins", item);

                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.Created)
                    {
                        Skin newSkin = await response.Content.ReadFromJsonAsync<Skin>();

                        Console.WriteLine("Skin {0} inserted", newSkin.Name);
                        return newSkin;
                    }
                    else
                    {
                        Console.WriteLine($"Failed to add item. Status code: {response.StatusCode}. Reason: {response.ReasonPhrase}");
                        return null;
                    }
                }
                catch (HttpRequestException ex)
                {
                    Console.WriteLine($"Failed to add item. {ex.Message}");
                    return null;
                }
                catch (JsonException ex)
                {
                    Console.WriteLine($"Failed to add item. Error while serializing JSON data. {ex.Message}");
                    return null;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Failed to add item. {ex.Message}");
                    return null;
                }
            }

            public async Task<bool> DeleteItem(Skin? item)
            {
                try
                {
                    if (item == null) throw new ArgumentNullException("Skin is null cannot delete empty");

                    var response = await parent.HttpClient.DeleteAsync($"/skins/{item.Name}");

                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("Skin {0} deleted", item.Name);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine($"Failed to delete item. Status code: {response.StatusCode}. Reason: {response.ReasonPhrase}");
                        return false;
                    }
                }
                catch (HttpRequestException ex)
                {
                    Console.WriteLine($"Failed to delete item. {ex.Message}");
                    return false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Failed to delete item. {ex.Message}");
                    return false;
                }
            }

            public async Task<IEnumerable<Skin?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                try
                {
                    string uri = $"/skins?index={index}&count={count}";
                    if (!string.IsNullOrEmpty(orderingPropertyName))
                    {
                        uri += $"&orderingPropertyName={orderingPropertyName}&descending={descending}";
                    }

                    var response = await parent.HttpClient.GetAsync(uri);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<Skin> skins = await response.Content.ReadFromJsonAsync<IEnumerable<Skin>>();
                        return skins;
                    }
                    else
                    {
                        Console.WriteLine($"Failed to get items. Status code: {response.StatusCode}. Reason: {response.ReasonPhrase}");
                        return Enumerable.Empty<Skin>();
                    }
                }
                catch (HttpRequestException ex)
                {
                    Console.WriteLine($"Failed to get items. {ex.Message}");
                    return Enumerable.Empty<Skin>();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Failed to get items. {ex.Message}");
                    return Enumerable.Empty<Skin>();
                }
            }

            public async Task<IEnumerable<Skin?>> GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                try
                {
                    if (champion == null) throw new ArgumentNullException(nameof(champion), "Champion is null");

                    // Build the query string with the specified parameters
                    string queryString = $"?champion={champion.Name}&index={index}&count={count}";
                    if (!string.IsNullOrEmpty(orderingPropertyName)) queryString += $"&orderingPropertyName={orderingPropertyName}&descending={descending}";

                    var response = await parent.HttpClient.GetAsync($"/skins{queryString}");

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<SkinDto> skins = await response.Content.ReadFromJsonAsync<IEnumerable<SkinDto>>();
                        IEnumerable<Skin> res = skins.Select(s => s.ToModel());

                        return res;

                    }
                    else
                    {
                        // Log the error status code and reason phrase
                        Console.WriteLine($"Failed to get items by champion. Status code: {response.StatusCode}. Reason: {response.ReasonPhrase}");
                        return null;
                    }
                }
                catch (HttpRequestException ex)
                {
                    // Log the error message from the HttpClient exception
                    Console.WriteLine($"Failed to get items by champion. {ex.Message}");
                    return null;
                }
                catch (JsonException ex)
                {
                    // Log the error message from the JSON serialization exception
                    Console.WriteLine($"Failed to get items by champion. Error while deserializing JSON data. {ex.Message}");
                    return null;
                }
                catch (Exception ex)
                {
                    // Log any other exceptions that may occur
                    Console.WriteLine($"Failed to get items by champion. {ex.Message}");
                    return null;
                }
            }

            public Task<IEnumerable<Skin?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItems()
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByChampion(Champion? champion)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByName(string substring)
            {
                throw new NotImplementedException();
            }

            public Task<Skin?> UpdateItem(Skin? oldItem, Skin? newItem)
            {
                throw new NotImplementedException();
            }
        }
    }
}
