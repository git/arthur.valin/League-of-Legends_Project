using API_LoL_Project.Controllers;
using API_LoL_Project.Controllers.Request;
using API_LoL_Project.Controllers.Response;
using API_LoL_Project.Controllers.version2;
using API_LoL_Project.Middleware;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Model;
using Shared;
using StubLib;
using System.Collections.ObjectModel;
using System.Net;
using System.Xml.Linq;

namespace Test_Api
{
    [TestClass]
    public class ChampionControllerTest
    {
        public readonly ChampionsController championCtrl;
        private readonly ILogger<ChampionsController> logger = new NullLogger<ChampionsController>();

        public readonly IDataManager stubMgr;
        public ChampionControllerTest()
        {
            stubMgr = new StubData();

            championCtrl = new ChampionsController(stubMgr, logger);
        }

        [TestMethod]
        public async Task TestGetSuccesChampions()
        {
            var totalcountTest = await stubMgr.ChampionsMgr.GetNbItems();

            // Arrange
            var request = new PageRequest
            {
                index = 0,
                count = 10,
                orderingPropertyName = "Name",
                descending = false
            };

            // Create some test data
            var champions = new List<Champion>
    {
       
        new Champion("Brand", ChampionClass.Mage),
        new Champion("Caitlyn", ChampionClass.Marksman),
        new Champion("Cassiopeia", ChampionClass.Mage)
    };
            foreach (var c in champions)
            {
                await stubMgr.ChampionsMgr.AddItem(c);
            }

            // Act
            var getResult = await championCtrl.Get(request);

            // Assert

            Assert.IsInstanceOfType(getResult.Result, typeof(OkObjectResult));
            var objectRes = getResult.Result as OkObjectResult;
            Assert.AreEqual(200, objectRes.StatusCode);
            Assert.IsNotNull(objectRes);

            var pageResponse = objectRes.Value as PageResponse<ChampionDTO>;
            Assert.IsNotNull(pageResponse);
            Assert.IsInstanceOfType(pageResponse, typeof(PageResponse<ChampionDTO>));
            Assert.AreEqual(totalcountTest + champions.Count, pageResponse.TotalCount);
            Assert.AreEqual(totalcountTest + champions.Count, pageResponse.Data.Count());

            var endpoints = pageResponse.Data.Select(r => r.Links);
            foreach (var endpointList in endpoints)
            {
                Assert.AreEqual(4, endpointList.Count());
                Assert.IsTrue(endpointList.Any(e => e.Href.Contains("/api/[controller]/")));
            }

            var championsDTO = pageResponse.Data.Select(r => r.Data);
            foreach (var c in championsDTO)
            {
                Assert.IsInstanceOfType(c, typeof(ChampionDTO));
            }
        }

            [TestMethod]
    public async Task TestGetChampions_When_Count_Index_Too_High()
    {

        var totalcountTest = await stubMgr.ChampionsMgr.GetNbItems();


        var request = new PageRequest
        {
            index = 999,
            count = 9,
            orderingPropertyName = "Name",
            descending = false
        };
        // Act
        var getResult = await championCtrl.Get(request);
        // Assert
        var badRequestResult = getResult.Result as BadRequestObjectResult;
        Assert.IsNotNull(badRequestResult);
        Assert.AreEqual($"To many object is asked the max is : {totalcountTest}", badRequestResult.Value);
    }


   [TestMethod]
    public async Task TestGetBadRequest_WhenNoChampionsFound()
    {

            // need to be empty
            List<Champion> champions = new()
        {
            new Champion("Akali", ChampionClass.Assassin),
            new Champion("Aatrox", ChampionClass.Fighter),
            new Champion("Ahri", ChampionClass.Mage),
            new Champion("Akshan", ChampionClass.Marksman),
            new Champion("Bard", ChampionClass.Support),
            new Champion("Alistar", ChampionClass.Tank),
        };
        foreach(var c in champions)
        {
            stubMgr.ChampionsMgr.DeleteItem(c);
        }
            

        var request = new PageRequest
        {
            index = 0,
            count = 10,
            orderingPropertyName = "Name",
            descending = false
        };


        // Act

        var getResult = await championCtrl.Get(request);
        Console.WriteLine(getResult);
        // Assert
        var badRequestResult = getResult.Result as BadRequestObjectResult;
        Assert.IsNotNull(badRequestResult);
        Assert.AreEqual("To many object is asked the max is : 0", badRequestResult.Value);
    }

        [TestMethod]
        public async Task TestGetChampionByName_ExistingName()
        {
            // Arrange

            var championName = "lopalinda";
            var champion = new Champion(championName, ChampionClass.Assassin);
            await stubMgr.ChampionsMgr.AddItem(champion);
            // Act
            var getResult = await championCtrl.GetChampionsByName(championName);

            // Assert
   /*         Assert.IsInstanceOfType(getResult, typeof(OkObjectResult));
            var objectRes = getResult.Result as OkObjectResult;
            Assert.AreEqual(200, objectRes.StatusCode);
            Assert.IsInstanceOfType(okResult.Value, typeof(IEnumerable<LolResponse<ChampionFullDTO>>));
            Assert.IsNotNull(objectRes);*/


            Assert.IsInstanceOfType(getResult.Result, typeof(OkObjectResult));
            var okResult = getResult.Result as OkObjectResult;
            Assert.IsInstanceOfType(okResult.Value, typeof(IEnumerable<LolResponse<ChampionFullDTO>>));
            var responseList = okResult.Value as IEnumerable<LolResponse<ChampionFullDTO>>;
            Assert.AreEqual(1, responseList.Count());
            var response = responseList.First();
            Assert.AreEqual(champion.Name, response.Data.Name);
        }
        
        
        [TestMethod]
        public async Task TestGetChampionsByName_ReturnsBadRequest_WhenNameIsEmpty()
        {
            // Arrange
            var name = "";

            // Act
            var result = await championCtrl.GetChampionsByName(name);

            // Assert
            Assert.IsInstanceOfType(result.Result, typeof(BadRequestObjectResult));
            var badRequestResult = result.Result as BadRequestObjectResult;
            Assert.AreEqual("Can not get champions without the name (is empty)", badRequestResult.Value);
        }
        [TestMethod]
        public async Task TestGetChampionsByName_ReturnsBadRequest_WhenNoChampionsFound()
        {
            // Arrange
            var name = "NonExistentChampionName";

            // Act
            var result = await championCtrl.GetChampionsByName(name);

            // Assert
            Assert.IsInstanceOfType(result.Result, typeof(NotFoundObjectResult));
            var badRequestResult = result.Result as NotFoundObjectResult;
            Assert.AreEqual("No chamions found with this name: NonExistentChampionNamein the dataContext", badRequestResult.Value);
        }


        /*[TestMethod]
        public async Task TestPostChampionsSucess()
        {
            var championName = "bhbhb";
            var expectedChampion = new ChampionFullDTO()
            {
                Characteristics = new ReadOnlyDictionary<string, int>(new Dictionary<string, int>() {
        { "health", 500 },
        { "attack damage", 60 },
        { "ability power", 0 }
    }),
                Name = championName,
                Bio = "Garen is a strong and noble warrior, the pride of Demacia.",
                Class = ChampionClass.Fighter,
                Icon = "https://example.com/garen-icon.png",
                LargeImage = new ImageDTO() { base64 = "xyz" },
                Skins = new List<SkinDto>() {
        new SkinDto() {
            Name = "Classic Garen",
            Description = "The default skin for Garen.",
            Icon = "https://example.com/garen-classic-icon.png",
            Price = 1350,
            LargeImage = new ImageDTO() { base64 = "xyz" }
        },
        new SkinDto() {
            Name = "God-King Garen",
            Description = "Garen as a divine being.",
            Icon = "https://example.com/garen-god-king-icon.png",
            Price = 1820,
            LargeImage = new ImageDTO() { base64 = "xyz" }
        }
    },
                Skills = new List<Skill>() {
        new Skill("Decisive Strike", SkillType.Basic, "Garen's next attack strikes a vital area of his foe, dealing bonus damage and silencing them."),
        new Skill("Courage", SkillType.Basic, "Garen passively increases his armor and magic resistance by killing enemies. He may also activate this ability to gain additional defensive statistics."),
        new Skill("Judgment", SkillType.Basic, "Garen rapidly spins his sword around his body, dealing physical damage to nearby enemies and reducing the duration of all incoming crowd control effects."),
        new Skill("Demacian Justice", SkillType.Ultimate, "Garen calls upon the might of Demacia to deal a finishing blow to an enemy champion, dealing massive damage based on their missing health.")
    }
            };
            var rep = await championCtrl.Post(expectedChampion);

          

            Assert.AreEqual(200, objectRes.StatusCode);

            Assert.IsNotNull(objectRes);

            var champions = objectRes?.Value as IEnumerable<ChampionDTO>;

            Assert.IsNotNull(champions);


        }
        */
        [TestMethod]
        public async Task TestPost_ReturnsCreatedAndSavedChampion()
        {
            // Arrange
            var expectedChampion = new ChampionFullDTO()
            {
                Characteristics = new ReadOnlyDictionary<string, int>(new Dictionary<string, int>() {
            { "health", 500 },
            { "attack damage", 60 },
            { "ability power", 0 }
        }),
                Name = "iuhbbhs",
                Bio = "Garen is a strong and noble warrior, the pride of Demacia.",
                Class = ChampionClass.Fighter,
                Icon = "https://example.com/garen-icon.png",
                LargeImage = new ImageDTO() { base64 = "xyz" },
                Skins = new List<SkinDto>() {
            new SkinDto() {
                Name = "Classic Garen",
                Description = "The default skin for Garen.",
                Icon = "https://example.com/garen-classic-icon.png",
                Price = 1350,
                LargeImage = new ImageDTO() { base64 = "xyz" }
            },
            new SkinDto() {
                Name = "God-King Garen",
                Description = "Garen as a divine being.",
                Icon = "https://example.com/garen-god-king-icon.png",
                Price = 1820,
                LargeImage = new ImageDTO() { base64 = "xyz" }
            }
        },
                Skills = new List<Skill>() {
            new Skill("Decisive Strike", SkillType.Basic, "Garen's next attack strikes a vital area of his foe, dealing bonus damage and silencing them."),
            new Skill("Courage", SkillType.Basic, "Garen passively increases his armor and magic resistance by killing enemies. He may also activate this ability to gain additional defensive statistics."),
            new Skill("Judgment", SkillType.Basic, "Garen rapidly spins his sword around his body, dealing physical damage to nearby enemies and reducing the duration of all incoming crowd control effects."),
            new Skill("Demacian Justice", SkillType.Ultimate, "Garen calls upon the might of Demacia to deal a finishing blow to an enemy champion, dealing massive damage based on their missing health.")
        }
            };

            // Act
            var postResult = await championCtrl.Post(expectedChampion);

            // Assert
            Assert.IsInstanceOfType(postResult, typeof(CreatedAtActionResult));
            var objectRes = postResult as CreatedAtActionResult;
            Assert.AreEqual(201, objectRes.StatusCode);
            Assert.IsNotNull(objectRes);
            Assert.AreEqual(nameof(championCtrl.Get), objectRes.ActionName);
            var createdChampion = objectRes.Value as ChampionDTO;
            Assert.IsNotNull(createdChampion);
            Assert.AreEqual(expectedChampion.Name, createdChampion.Name);
            Assert.AreEqual(expectedChampion.Class, createdChampion.Class);
        }

        [TestMethod]
        public async Task TestPutChampion()
        {
            // Arrange
            var championName = "kakarot";
            var updatedChampion = new ChampionDTO
            {
                Name = championName,
                Class = ChampionClass.Mage,
                Icon = "dsodm",
                Bio = "totto",
            };

            await stubMgr.ChampionsMgr.AddItem(new Champion(championName, ChampionClass.Assassin));
            // Act
            var result = await championCtrl.Put(championName, updatedChampion);

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            var champion = await stubMgr.ChampionsMgr.GetItemsByName(championName, 0, 1);
            Assert.AreEqual(updatedChampion.Name, champion.First().Name);
            Assert.AreEqual(updatedChampion.Class, champion.First().Class);
            Assert.AreEqual(updatedChampion.Bio, champion.First().Bio);
        }

        [TestMethod]
        public async Task TestDeleteChampion()
        {
            // Arrange
            var championToDelete = new Champion("Yasuo", ChampionClass.Fighter);
            await stubMgr.ChampionsMgr.AddItem(championToDelete);

            // Act
            var result = await championCtrl.Delete(championToDelete.Name);

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkResult));
            Assert.AreEqual(200, (result as OkResult).StatusCode);

            var championsAfterDelete = await stubMgr.ChampionsMgr.GetItems(0, await stubMgr.ChampionsMgr.GetNbItems());
            Assert.IsFalse(championsAfterDelete.Any(c => c.Name == championToDelete.Name));
        }

        [TestMethod]
        public async Task TestDeleteChampionWithInvalidName()
        {
            // Arrange
            var invalidName = "Invalid Name";

            // Act
            var result = await championCtrl.Delete(invalidName);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundObjectResult));
            Assert.AreEqual(404, (result as NotFoundObjectResult).StatusCode);
            Assert.AreEqual($"No chamions found with {invalidName} cannot delete", (result as NotFoundObjectResult).Value);
        }

        [TestMethod]
        public async Task TestDeleteChampionWithEmptyName()
        {
            // Arrange
            var emptyName = string.Empty;

            // Act
            var result = await championCtrl.Delete(emptyName);

            // Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual(400, (result as BadRequestObjectResult).StatusCode);
            Assert.AreEqual("Can not delelte champions without the name (is empty)", (result as BadRequestObjectResult).Value);
        }




    }
}