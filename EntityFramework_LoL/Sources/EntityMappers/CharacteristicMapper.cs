﻿using Entities;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityMappers
{
    public static class CharacteristicMapper
    {
        public static CharacteristicEntity ToEntity(this KeyValuePair<string, int> item, ChampionEntity champion, LolDbContext context)
        {
            var characteristicEntity = context.characteristics.Find(item.Key, champion.Name);
            if (characteristicEntity == null)
            {
                return new()
                {
                    Name = item.Key,
                    Value = item.Value,
                    ChampionForeignKey = champion.Name
                };
            }
            return characteristicEntity;
        }


        public static Tuple<string, int> ToModel(this CharacteristicEntity entity)
        => new(entity.Name, entity.Value);
    }
}

