﻿using Entities;
using Model;
using System.Reflection.Metadata.Ecma335;

namespace EntityMapper
{
    public static class RuneMapper
    {
        public static RuneEntity ToEntity(this Rune item, LolDbContext context)
        {
            RuneEntity? runeEntity = context.runes.Find(item.Name);
            if (runeEntity == null) { 
                return new()
                {
                    Name = item.Name,
                    Description = item.Description,
                    RuneFamily = item.Family,
                    Icon = item.Icon,
                    Image = new() { Base64 = item.Image.Base64 },
                };
            }
            return runeEntity;
        }


        public static Rune ToModel(this RuneEntity entity)
        {
            var image = entity?.Image?.Base64 ?? "";
            return new Rune(entity?.Name ?? "", entity?.RuneFamily??Shared.RuneFamily.Unknown, entity?.Icon ?? "", image, entity?.Description??"");
        }

    }
}   


