﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Model;

namespace EntityMapper
{
    public static class SkinMapper
    {
        public static SkinEntity ToEntity(this Skin item, LolDbContext? context = null)
        {
            return new()
            {
                Name = item.Name,
                Champion = context?.champions.Find(item.Champion.Name) ?? item.Champion.ToEntity(context),
                ChampionForeignKey = item.Champion.Name,
                Description = item.Description,
                Icon = item.Icon,
                Image = new() { Base64 = item.Image.Base64 },
                Price = item.Price
            };
        }


        public static Skin ToModel(this SkinEntity entity)
        {
            var image = entity?.Image?.Base64 ?? "";
            return new(entity?.Name ?? "", entity?.Champion?.ToModel()??new(""), entity?.Price??-1, image, entity?.Description??"");
        }

    }
}
