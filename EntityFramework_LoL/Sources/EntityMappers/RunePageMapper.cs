﻿using Entities;
using Model;
using Shared;

namespace EntityMapper
{
    public static class RunePageMapper
    {
        public static RunePageEntity ToEntity(this RunePage item, LolDbContext context)
        {
            RunePageEntity? runePageEntity = context.runepages.Find(item.Name);
            if (runePageEntity == null)
            {
                runePageEntity = new()
                {
                    Name = item.Name,
                };

                runePageEntity.entries = new List<RunePageRuneEntity>();
                foreach (var r in item.Runes)
                {
                    runePageEntity.entries.Add(new RunePageRuneEntity()
                    {
                        category = r.Key,
                        rune = r.Value.ToEntity(context),
                    });
                }

            }
            return runePageEntity;
        }

        public static RunePage ToModel(this RunePageEntity entity, LolDbContext context)
        {
            RunePage runePage = new(entity.Name);
            if(entity.entries!= null)
            {
                foreach(var r in entity.entries)
                {
                    var rune = context.runes.Find(r.RuneName);
                    if(rune!=null) runePage[r.category] = rune.ToModel();
                };
            }
            return runePage;

        }
    }
}
