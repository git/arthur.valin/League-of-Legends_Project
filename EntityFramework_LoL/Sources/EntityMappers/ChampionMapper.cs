﻿using Entities;
using EntityMappers;
using Model;

namespace EntityMapper
{
    public static class ChampionMapper { 
    
        public static ChampionEntity ToEntity(this Champion item, LolDbContext context)
        {

            ChampionEntity? championEntity = context.champions.Find(item.Name);
            if (championEntity == null)
            {
                championEntity = new()
                {
                    Name = item.Name,
                    Bio = item.Bio,
                    Icon = item.Icon,
                    Class = item.Class,
                    Image = new() { Base64 = item.Image.Base64 },
                };
                championEntity.Skills = item.Skills.Select(x => x.ToEntity(championEntity, context)).ToList();
                championEntity.Characteristics = item.Characteristics.Select(x => x.ToEntity(championEntity, context)).ToList();

            }
            return championEntity;

        }

        public static Champion ToModel(this ChampionEntity entity)
        {
            var image = entity?.Image?.Base64 ?? "";
            var champion =  new Champion(entity?.Name ?? "", entity?.Class??Shared.ChampionClass.Unknown, entity?.Icon??"", image , entity?.Bio??"");
            if(entity.Skills!=null) foreach(var s in entity.Skills){champion.AddSkill(s.ToModel());}
            if (entity.Characteristics != null) foreach (var c in entity.Characteristics){champion.AddCharacteristics(c.ToModel()); }
            return champion;
        }

    }
}
