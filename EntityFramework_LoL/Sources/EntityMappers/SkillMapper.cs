﻿using Entities;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityMappers
{
    public static class SkillMapper
    {
        public static SkillEntity ToEntity(this Skill item, ChampionEntity champion, LolDbContext context)
        {
            var skillEntity = context.skills.Find(item.Name);
            if (skillEntity == null) {
                return new()
                {
                    Name = item.Name,
                    Description = item.Description,
                    SkillType = item.Type,
                    Champions = new List<ChampionEntity>() { champion }
                };
            }
            skillEntity!.Champions?.Add(champion);
            return skillEntity;
        }


        public static Skill ToModel(this SkillEntity entity)
        => new(entity.Name, entity.SkillType,entity.Description);
    }
}
