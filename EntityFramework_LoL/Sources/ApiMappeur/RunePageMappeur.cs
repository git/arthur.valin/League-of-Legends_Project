﻿using DTO;
using Shared;
using Rune = Model.Rune;
using Model;

namespace ApiMappeur
{
    /* public static class RunePageMappeur
     {
         public static RunePageDTO ToDto(this RunePage runePage)
         {
             var runePageDTO = new RunePageDTO
             {
                 Name = runePage.Name,
                 Runes = new Dictionary<Category, RuneDTO>()
             };
             foreach (var runeCategory in Enum.GetValues(typeof(Category)).Cast<Category>())
             {
                 var rune = runePage[runeCategory];
                 if (rune != null)
                 {
                     runePageDTO.Runes[runeCategory] = rune.ToDTO();
                 }
             }
             return runePageDTO;
         }

         public static RunePage ToModel(this RunePageDTO runePageDto)
         {
             var entity = new RunePage(runePageDto.Name);

             foreach (var kvp in runePageDto.Runes)
             {
                 var category = kvp.Key;
                 var runeDTO = kvp.Value;
                 var rune = new Rune(runeDTO.Name, runeDTO.Family, runeDTO.Icon, runeDTO.Image.Base64, runeDTO.Description);
                 entity[category] = rune;
             }

             return entity;}
         }*/


    public static class RunePageMappeur
    {
        public static RunePageDTO ToDto(this RunePage runePage)
        {

            return new RunePageDTO()
            {
                Name = runePage.Name,
                Runes = runePage.Runes.ToDictionary(c => c.Key.ToString(), r => r.Value.ToDTO())
            };
        }

        public static RunePage ToModel(this RunePageDTO runePageDto)
        {
            Category category;
            Dictionary<Category, Rune> runDico = runePageDto.Runes.ToDictionary(
                r => (Category)Enum.Parse(typeof(Category), r.Key),
                r => r.Value.ToModel()
            );

            var runePage = new RunePage(runePageDto.Name);
            foreach (var rune in runePageDto.Runes)
            {
                if (!Enum.TryParse<Category>(rune.Key, true, out category))
                {
                    continue;
                }
                runePage[category] = rune.Value.ToModel();
            }

            return runePage;
        }
    }
}
