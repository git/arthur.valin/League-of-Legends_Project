﻿using DTO;
using Entities;
using Model;

namespace ApiMappeur
{
    public static class ChampionMapper
    {

        public static ChampionDTO ToDTO(this Champion item)
        {
            
            return new ChampionDTO()
            {
                Icon = item.Icon,
                Name = item.Name,
                Bio = item.Bio,
                Class = item.Class
            };
        }

        public static ChampionEntity ToEntity(this Champion item)
        {
            return new()
            {
                Name = item.Name,
                Bio = item.Bio,
                Icon = item.Icon,
                Class = item.Class,
                Image = new() { Base64 = item.Image.Base64 },
            };
        }
        public static ChampionFullDTO toFullDTO(this Champion item)
        {
            if (item == null)
            {
                var message = string.Format("Champion is null cannot null it");

                throw new Exception(message);

            }
            return new ChampionFullDTO()
            {
                Name = item.Name,
                Characteristics = item.Characteristics,
                Bio = item.Bio,
                Skills = item.Skills,
                Class = item.Class,
                Skins = item.Skins.Select(i => i.ToDto()),
                LargeImage = item.Image.ToDTO(),
                Icon = item.Icon,
                
            };

        }

        public static Champion ToModel(this ChampionFullDTO dto)
        {
            if (dto == null)
            {
                var message = string.Format("Champion cannot be empty");
                throw new Exception(message);

            }
            var champion =  new Champion(dto.Name, dto.Class, dto.Icon,dto?.LargeImage?.base64 ?? "", dto.Bio);

            champion.AddCharacteristics(dto.Characteristics?.Select(kv => Tuple.Create(kv.Key, kv.Value)).ToArray() ?? Array.Empty<Tuple<string, int>>());

            if (dto.Skills != null)
            {
                foreach (var skill in dto.Skills)
                {
                    champion.AddSkill(skill);
                }
            }

           /* if (dto.Skins != null)
            {
                foreach (var skinDto in dto.Skins)
                {
                    var skin = new Skin(skinDto.Name, dto.ToModel());
                    champion.AddSkin(skin);
                }
            }*/

            return champion;

        }
        public static Champion ToModel(this ChampionDTO dto)
        {
            if (dto == null)
            {
                var message = string.Format("Champion cannot be empty");
                /*throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            */
                throw new Exception(message);

            }
            return new Champion(dto.Name, dto.Class, dto.Icon)
            {
                Bio = dto.Bio
            };
        }

        public static Champion ToModel(this ChampionEntity entity)
        {
            return new(entity.Name, entity.Class, entity.Icon, entity.Image.Base64, entity.Bio);
        }



    }
}
