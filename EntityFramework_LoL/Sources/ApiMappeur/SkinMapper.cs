﻿using DTO;
using Entities;
using Model;

namespace ApiMappeur
{
    public static class SkinMapper
    {


        public static Skin ToModel(this SkinEntity entity)
        {
            throw new NotImplementedException();

        }
        public static SkinDto ToDto(this Skin item)
        {
            return new SkinDto()
            {
                Name = item.Name,
                LargeImage = item.Image.ToDTO(),
                Description = item.Description,
                Icon = item.Icon,
                Price = item.Price,
            };

        }
        public static Skin ToModel(this SkinDto item)
        {

                var image = item?.LargeImage?.base64 ?? "";
                return new(item?.Name ?? "", null, item?.Price ?? -1, image, item?.Description ?? "");
        }

    }
}

