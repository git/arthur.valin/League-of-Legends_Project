﻿    using DTO;
    using Model;
using System.Text;
using Rune = Model.Rune;


namespace ApiMappeur
{
        public static class RuneMapper
        {
            public static RuneDTO ToDTO(this Rune item)
            {

            return new RuneDTO
            {
                Name = item.Name,
                Description = item.Description,
                Family = item.Family,
                Icon = item.Icon,
                Image = item.Image
            };
        }

            public static Rune ToModel(this RuneDTO dto)
            {
            /*if (dto == null)
            {
               *//* var message = string.Format("Champion with name = {} not found", dto.Name);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, message));*//*
            }*/
            return new Rune(dto.Name, dto.Family, dto.Icon, dto.Image.Base64, dto.Description);
        }
        }
    }



