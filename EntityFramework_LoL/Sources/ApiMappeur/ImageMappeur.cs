﻿using DTO;
using Model;

namespace ApiMappeur
{
    public static class ImageMappeur
    {
        public static ImageDTO ToDTO(this LargeImage item)
        {
            if (item == null)
            {
                var message = string.Format("Image cannot be empty");
                /*throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            */
            }
            return new ImageDTO()
            {
                base64 = item?.Base64 ?? ""
            };
        }

    }
}
