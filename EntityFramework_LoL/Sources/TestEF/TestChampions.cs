using Business;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using Shared;
using Xunit.Abstractions;

namespace TestEF
{
    public class TestChampions
    {

        [Fact]
        public async void Test_Add()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
            .UseSqlite(connection)
            .Options;

            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();

                Champion batman = new("Batman", ChampionClass.Assassin, "icon_1", "image_1", "L'ombre de la nuit");
                batman.AddSkill(new("Bat-signal", SkillType.Basic, "Envoie le signal"));
                batman.AddCharacteristics(new[] {
                    Tuple.Create("Force", 150),
                    Tuple.Create("Agilit�", 500)
                });
                Champion endeavor = new("Endeavor", ChampionClass.Tank, "icon_2", "image_2", "Feu br�lant �nernel");
                endeavor.AddSkill(new("Final flames", SkillType.Ultimate, "Derni�re flamme d'un h�ro"));
                endeavor.AddCharacteristics(new[] {
                    Tuple.Create("Force", 200),
                    Tuple.Create("D�fense", 300),
                    Tuple.Create("Alter", 800)
                });


                Champion escanor = new("Escanor", ChampionClass.Fighter, "icon_3", "image_3", "1, 2, 3, Soleil");
                escanor.AddSkill(new("Croissance solaire", SkillType.Passive, "Le soleil rends plus fort !"));
                escanor.AddCharacteristics(new[] {
                    Tuple.Create("Force", 500),
                    Tuple.Create("D�fense", 500)
                });

                await manager.AddItem(batman);
                await manager.AddItem(endeavor);
                await manager.AddItem(escanor);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();

                var nbItems = await manager.GetNbItems();
                Assert.Equal(3, nbItems);

                var items = await manager.GetItemsByName("Batman", 0, nbItems);
                Assert.Equal("Batman", items.First().Name);


                Assert.Equal(2, await manager.GetNbItemsByName("E"));

                items = await manager.GetItemsBySkill("Croissance solaire", 0, nbItems);
                Assert.Equal("Escanor", items.First().Name);

                items = await manager.GetItemsBySkill(new Skill("Final flames", SkillType.Ultimate, "Derni�re flamme d'un h�ro"), 
                                                      0, nbItems);
                Assert.Equal("Endeavor", items.First().Name);

                items = await manager.GetItemsByCharacteristic("Alter", 0, nbItems);
                Assert.Equal("Endeavor", items.First().Name);

                Assert.Equal(2, await manager.GetNbItemsByCharacteristic("D�fense"));
            }

        }

        [Fact]
        public async void Test_Update()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).ChampionsMgr;
                
                context.Database.EnsureCreated();

                Champion batman = new("Batman", ChampionClass.Assassin, "icon_1", "image_1", "L'ombre de la nuit");
                Champion endeavor = new("Endeavor", ChampionClass.Tank, "icon_2", "image_2", "Feu br�lant �nernel");
                Champion escanor = new("Escanor", ChampionClass.Fighter, "icon_3", "image_3", "1, 2, 3, Soleil");

                await manager.AddItem(batman);
                await manager.AddItem(endeavor);
                await manager.AddItem(escanor);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();


                var items = await manager.GetItemsByName("E", 0, 3);
                Assert.Equal(2, items.Count());

                var escanor = context.champions.Where(n => n.Name.Contains("Esc")).First();
                escanor.Class = ChampionClass.Tank;
                context.SaveChanges();

                items = await manager.GetItemsByClass(ChampionClass.Tank, 0, 3);
                Assert.Contains("Escanor", items.Select(x => x.Name));

                Assert.Equal(2, await manager.GetNbItemsByClass(ChampionClass.Tank));

            }
        }

        [Fact]
        public async void Test_Delete()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();


                Champion batman = new("Batman", ChampionClass.Assassin, "icon_1", "image_1", "L'ombre de la nuit");
                batman.AddSkill(new("Charge", SkillType.Basic, "Coup de base"));
                batman.AddSkill(new("Double Saut", SkillType.Basic, ""));

                Champion endeavor = new("Endeavor", ChampionClass.Tank, "icon_2", "image_2", "Feu br�lant �nernel");
                endeavor.AddSkill(new("Charge", SkillType.Basic, "Coup de base"));

                Champion escanor = new("Escanor", ChampionClass.Fighter, "icon_3", "image_3", "1, 2, 3, Soleil");
                escanor.AddSkill(new("Charge", SkillType.Basic, "Coup de base"));
                batman.AddSkill(new("Double Saut", SkillType.Basic, ""));

                await manager.AddItem(batman);
                await manager.AddItem(endeavor);
                await manager.AddItem(escanor);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();

                var endeavor = (await manager.GetItemsByName("Endeavor", 0, 3)).First();

                var itemsByName = await manager.DeleteItem(endeavor);
                Assert.Equal(2, await manager.GetNbItems());

                var items = await manager.GetItems(0, await manager.GetNbItems());
                Assert.DoesNotContain("Endeavor", items.Select(x => x.Name));

                Assert.Equal(1, await manager.GetNbItemsBySkill(new Skill("Double Saut", SkillType.Basic, "")));
                Assert.Equal(2, await manager.GetNbItemsBySkill("Charge"));

            }

        }

    }
}