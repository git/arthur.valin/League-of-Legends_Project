﻿using Business;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEF
{
    public class TestRunePage
    {
        [Fact]
        public async void Test_Add()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
            .UseSqlite(connection)
            .Options;

            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunePagesMgr;
                var runeManager = new DbData(context).RunesMgr;
                var championManager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();

                Model.Rune rune1 = new("Sanglante", RuneFamily.Domination);
                Model.Rune rune2 = new("Oeil de l'esprit", RuneFamily.Precision);
                Model.Rune rune3 = new("Concrétisation", RuneFamily.Unknown);
                Model.Rune rune4 = new("The moon", RuneFamily.Unknown);
                Model.Rune rune5 = new("Radiance", RuneFamily.Domination);
                Model.Rune rune6 = new("Bullseye", RuneFamily.Precision);

                RunePage runepage1 = new("Damages");
                runepage1[Category.Major] = rune1;
                runepage1[Category.Minor1] = rune2;
                runepage1[Category.Minor2] = rune5;

                RunePage runepage2 = new("Hawk");

                runepage2[Category.Major] = rune6;
                runepage2[Category.Minor1] = rune2;

                RunePage runepage3 = new("Juggler");
                runepage3[Category.Major] = rune5;
                runepage3[Category.Minor1] = rune3;
                runepage3[Category.Minor2] = rune2;

                await runeManager.AddItem(rune1);
                await runeManager.AddItem(rune2);
                await runeManager.AddItem(rune3);
                await runeManager.AddItem(rune4);
                await runeManager.AddItem(rune5);
                await runeManager.AddItem(rune6);

                await manager.AddItem(runepage1);
                await manager.AddItem(runepage2);
                await manager.AddItem(runepage3);

                Champion batman = new("Batman", ChampionClass.Assassin, "icon_1", "image_1", "L'ombre de la nuit");
                Champion endeavor = new("Endeavor", ChampionClass.Tank, "icon_2", "image_2", "Feu brûlant énernel");
                Champion escanor = new("Escanor", ChampionClass.Fighter, "icon_3", "image_3", "1, 2, 3, Soleil");

                await championManager.AddItem(batman);
                await championManager.AddItem(endeavor);
                await championManager.AddItem(escanor);


                var runepage_entities = context.runepages;
                var champion_entities = context.champions;

                var damages = runepage_entities.Find("Damages");
                var hawk = runepage_entities.Find("Hawk");
                var juggler = runepage_entities.Find("Juggler");

                champion_entities.Find("Batman")!.runepages = new List<RunePageEntity>() { hawk, juggler };
                champion_entities.Find("Endeavor")!.runepages = new List<RunePageEntity>() { damages };
                champion_entities.Find("Escanor")!.runepages = new List<RunePageEntity>() { damages };
                context.SaveChanges();

            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunePagesMgr;
                var championManager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();

                Model.Rune rune5 = new("Radiance", RuneFamily.Domination);
                Model.Rune rune1 = new("Sanglante", RuneFamily.Domination);

                Champion batman = new("Batman", ChampionClass.Assassin, "icon_1", "image_1", "L'ombre de la nuit");
                Champion endeavor = new("Endeavor", ChampionClass.Tank, "icon_2", "image_2", "Feu brûlant énernel");


                var nbItems = await manager.GetNbItems();
                Assert.Equal(3, nbItems);

                var items = await manager.GetItemsByName("Ha", 0, nbItems);
                Assert.Equal("Hawk", items.First().Name);

                Assert.Equal(2, await manager.GetNbItemsByName("a"));

                Assert.Equal(1, await manager.GetNbItemsByRune(rune5));

                items = await manager.GetItemsByRune(rune1, 0, nbItems);
                Assert.Equal("Damages", items.First().Name);

                Assert.Equal(2, await manager.GetNbItemsByChampion(batman));
                Assert.Equal("Damages", (await manager.GetItemsByChampion(endeavor, 0, 3)).First()!.Name);

                Assert.Equal(2, await championManager.GetNbItemsByRunePage(new("Damages")));
                Assert.Equal("Batman", (await championManager.GetItemsByRunePage(new("Juggler"), 0, 3)).First()!.Name);

            }
        }

        [Fact]
        public async void Test_Update()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunePagesMgr;
                var runeManager = new DbData(context).RunesMgr;

                context.Database.EnsureCreated();

                Model.Rune rune1 = new("Sanglante", RuneFamily.Domination);
                Model.Rune rune2 = new("Oeil de l'esprit", RuneFamily.Precision);
                Model.Rune rune3 = new("Concrétisation", RuneFamily.Unknown);
                Model.Rune rune4 = new("The moon", RuneFamily.Unknown);
                Model.Rune rune5 = new("Radiance", RuneFamily.Domination);
                Model.Rune rune6 = new("Bullseye", RuneFamily.Precision);

                RunePage runepage1 = new("Damages");
                runepage1[Category.Major] = rune1;
                runepage1[Category.Minor1] = rune2;
                runepage1[Category.Minor2] = rune5;

                RunePage runepage2 = new("Hawk");
                runepage2[Category.Major] = rune6;
                runepage2[Category.Minor1] = rune2;

                RunePage runepage3 = new("Juggler");
                runepage3[Category.Major] = rune5;
                runepage3[Category.Minor1] = rune3;
                runepage3[Category.Minor2] = rune2;

                await runeManager.AddItem(rune1);
                await runeManager.AddItem(rune2);
                await runeManager.AddItem(rune3);
                await runeManager.AddItem(rune4);
                await runeManager.AddItem(rune5);
                await runeManager.AddItem(rune6);

                await manager.AddItem(runepage1);
                await manager.AddItem(runepage2);
                await manager.AddItem(runepage3);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunePagesMgr;
                Model.Rune rune1 = new("Sanglante", RuneFamily.Domination);
                Model.Rune rune6 = new("Bullseye", RuneFamily.Precision);

                context.Database.EnsureCreated();

                Assert.Equal(1, await manager.GetNbItemsByRune(rune1));
                Assert.Equal(1, await manager.GetNbItemsByRune(rune6));

                RunePage after = new("Hawk");
                after[Category.Major] = rune1;

                await manager.UpdateItem(new("Hawk"), after);

                Assert.Equal(2, await manager.GetNbItemsByRune(rune1));
                Assert.Equal(0, await manager.GetNbItemsByRune(rune6));

                context.SaveChanges();
            }
        }

        [Fact]
        public async void Test_Delete()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunePagesMgr;
                var runeManager = new DbData(context).RunesMgr;

                context.Database.EnsureCreated();

                Model.Rune rune1 = new("Sanglante", RuneFamily.Domination);
                Model.Rune rune2 = new("Oeil de l'esprit", RuneFamily.Precision);
                Model.Rune rune3 = new("Concrétisation", RuneFamily.Unknown);
                Model.Rune rune4 = new("The moon", RuneFamily.Unknown);
                Model.Rune rune5 = new("Radiance", RuneFamily.Domination);
                Model.Rune rune6 = new("Bullseye", RuneFamily.Precision);

                RunePage runepage1 = new("Damages");
                runepage1[Category.Major] = rune1;
                runepage1[Category.Minor1] = rune2;
                runepage1[Category.Minor2] = rune5;

                RunePage runepage2 = new("Hawk");

                runepage2[Category.Major] = rune6;
                runepage2[Category.Minor1] = rune2;

                RunePage runepage3 = new("Juggler");
                runepage3[Category.Major] = rune5;
                runepage3[Category.Minor1] = rune3;
                runepage3[Category.Minor2] = rune2;

                await runeManager.AddItem(rune1);
                await runeManager.AddItem(rune2);
                await runeManager.AddItem(rune3);
                await runeManager.AddItem(rune4);
                await runeManager.AddItem(rune5);
                await runeManager.AddItem(rune6);

                await manager.AddItem(runepage1);
                await manager.AddItem(runepage2);
                await manager.AddItem(runepage3);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunePagesMgr;

                Model.Rune rune2 = new("Oeil de l'esprit", RuneFamily.Precision);


                context.Database.EnsureCreated();

                Assert.Equal(3, await manager.GetNbItems());

                await manager.DeleteItem(new("Juggler"));

                Assert.Equal(2, await manager.GetNbItems());

                Assert.Equal(1, await manager.GetNbItemsByName("e"));

                Assert.Equal(1, await manager.GetNbItemsByRune(rune2));


            }

        }

    }
}
