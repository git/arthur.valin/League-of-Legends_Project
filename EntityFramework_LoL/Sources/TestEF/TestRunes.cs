﻿using Business;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEF
{
    public class TestRunes
    {

        [Fact]
        public async void Test_Add()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
            .UseSqlite(connection)
            .Options;

            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunesMgr;

                context.Database.EnsureCreated();

                Model.Rune rune1 = new("Sanglante", RuneFamily.Domination);
                Model.Rune rune2 = new("Oeil de l'esprit", RuneFamily.Precision);
                Model.Rune rune3 = new("Concrétisation", RuneFamily.Unknown);

                await manager.AddItem(rune1);
                await manager.AddItem(rune2);
                await manager.AddItem(rune3);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunesMgr;

                context.Database.EnsureCreated();

                var nbItems = await manager.GetNbItems();
                Assert.Equal(3, nbItems);

                var items = await manager.GetItemsByName("Sanglante", 0, nbItems);
                Assert.Equal("Sanglante", items.First().Name);

                Assert.Equal(1, await manager.GetNbItemsByName("Concrétisation"));

                items = await manager.GetItemsByFamily(RuneFamily.Precision, 0, nbItems);
                Assert.Equal("Oeil de l'esprit", items.First().Name);

                Assert.Equal(1, await manager.GetNbItemsByFamily(RuneFamily.Unknown));
            }
        }


        [Fact]
        public async void Test_Update()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunesMgr;

                context.Database.EnsureCreated();


                Model.Rune rune1 = new("Sanglante", RuneFamily.Domination);
                Model.Rune rune2 = new("Oeil de l'esprit", RuneFamily.Precision);
                Model.Rune rune3 = new("Concrétisation", RuneFamily.Unknown);

                await manager.AddItem(rune1);
                await manager.AddItem(rune2);
                await manager.AddItem(rune3);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunesMgr;

                context.Database.EnsureCreated();

                Assert.Equal(1, await manager.GetNbItemsByFamily(RuneFamily.Precision));

                Model.Rune before = new("Concrétisation", RuneFamily.Unknown);
                Model.Rune after = new("Concrétisation", RuneFamily.Precision);

                await manager.UpdateItem(before, after);

                Assert.Equal(2, await manager.GetNbItemsByFamily(RuneFamily.Precision));

                context.SaveChanges();
            }
        }

        [Fact]
        public async void Test_Delete()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunesMgr;

                context.Database.EnsureCreated();

                Model.Rune rune1 = new("Sanglante", RuneFamily.Domination);
                Model.Rune rune2 = new("Oeil de l'esprit", RuneFamily.Precision);
                Model.Rune rune3 = new("Concrétisation", RuneFamily.Unknown);

                await manager.AddItem(rune1);
                await manager.AddItem(rune2);
                await manager.AddItem(rune3);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).RunesMgr;

                context.Database.EnsureCreated();

                Assert.Equal(3, await manager.GetNbItems());

                await manager.DeleteItem(new("Concrétisation", RuneFamily.Unknown));

                Assert.Equal(2, await manager.GetNbItems());

                Assert.Equal(0, await manager.GetNbItemsByName("Concrétisation"));

            }

        }
    }
}
