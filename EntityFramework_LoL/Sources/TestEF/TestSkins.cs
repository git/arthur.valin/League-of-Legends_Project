﻿using Business;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEF
{
    public class TestSkins
    {
        [Fact]
        public async void Test_Add()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
            .UseSqlite(connection)
            .Options;

            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).SkinsMgr;
                var championManager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();

                Champion batman = new("Batman", ChampionClass.Assassin);
                Champion endeavor = new("Endeavor", ChampionClass.Tank);
                await championManager.AddItem(batman);
                await championManager.AddItem(endeavor);

                Skin batman_skin_1 = new("Batman de glace", batman);
                Skin batman_skin_2 = new("Batman gold", batman);
                Skin batman_skin_3 = new("L'homme araignée", batman);
                Skin endeavor_skin_1 = new("L'abominable Endeavor", endeavor);

                await manager.AddItem(batman_skin_1);
                await manager.AddItem(batman_skin_2);
                await manager.AddItem(batman_skin_3);
                await manager.AddItem(endeavor_skin_1);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).SkinsMgr;

                context.Database.EnsureCreated();

                var nbItems = await manager.GetNbItems();
                Assert.Equal(4, nbItems);
                Assert.Equal(4, (await manager.GetItems(0, nbItems)).Count());

                var items = await manager.GetItemsByName("Batman", 0, nbItems);
                Assert.Equal(2, items.Count());

                Champion batman = new("Batman", ChampionClass.Assassin);
                items = await manager.GetItemsByChampion(batman, 0, nbItems);
                Assert.Equal(3, items.Count());
            }

        }

        [Fact]
        public async void Test_Update()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).SkinsMgr;
                var championManager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();

                Champion batman = new("Batman", ChampionClass.Assassin);
                Champion endeavor = new("Endeavor", ChampionClass.Tank);
                await championManager.AddItem(batman);
                await championManager.AddItem(endeavor);

                Skin batman_skin_1 = new("Batman de glace", batman);
                Skin batman_skin_2 = new("Batman gold", batman);
                Skin batman_skin_3 = new("L'homme araignée", batman);
                Skin endeavor_skin_1 = new("L'abominable Endeavor", endeavor);

                await manager.AddItem(batman_skin_1);
                await manager.AddItem(batman_skin_2);
                await manager.AddItem(batman_skin_3);
                await manager.AddItem(endeavor_skin_1);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).SkinsMgr;

                context.Database.EnsureCreated();


                Champion batman = new("Batman", ChampionClass.Assassin);
                Champion endeavor = new("Endeavor", ChampionClass.Tank);
                
                var itemsByName = await manager.GetItemsByChampion(batman, 0, 4);
                Assert.Equal(3, itemsByName.Count());

                Skin batman_skin = new("L'homme araignée", batman);
                Skin endeavor_skin = new("L'homme araignée", endeavor);

                await manager.UpdateItem(batman_skin, endeavor_skin);

                itemsByName = await manager.GetItemsByChampion(batman, 0, 4);
                Assert.Equal(2, itemsByName.Count());

                context.SaveChanges();
            }
        }

        [Fact]
        public async void Test_Delete()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LolDbContext>()
                .UseSqlite(connection)
                .Options;

            //prepares the database with one instance of the context
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).SkinsMgr;
                var championManager = new DbData(context).ChampionsMgr;

                context.Database.EnsureCreated();

                Champion batman = new("Batman", ChampionClass.Assassin);
                Champion endeavor = new("Endeavor", ChampionClass.Tank);
                await championManager.AddItem(batman);
                await championManager.AddItem(endeavor);

                Skin batman_skin_1 = new("Batman de glace", batman);
                Skin batman_skin_2 = new("Batman gold", batman);
                Skin batman_skin_3 = new("L'homme araignée", batman);
                Skin endeavor_skin_1 = new("L'abominable Endeavor", endeavor);

                await manager.AddItem(batman_skin_1);
                await manager.AddItem(batman_skin_2);
                await manager.AddItem(batman_skin_3);
                await manager.AddItem(endeavor_skin_1);
            }

            //uses another instance of the context to do the tests
            using (var context = new LolDbContext(options))
            {
                var manager = new DbData(context).SkinsMgr;

                context.Database.EnsureCreated();

                await manager.DeleteItem((await manager.GetItemsByName("L'", 0, 4)).First());
                await manager.DeleteItem((await manager.GetItemsByName("L'", 0, 3)).First());

                Assert.Equal(2, await manager.GetNbItems());

                Assert.Equal(2, await manager.GetNbItemsByName("Batman"));

                Champion batman = new("Batman", ChampionClass.Assassin);
                Assert.Equal(2, await manager.GetNbItemsByChampion(batman));
            }

        }
    }
}
