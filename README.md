  

![League Of Legends](./doc/images/LoL_Banner.png)

---

## Répartition du Git

La racine de notre git est composée de deux dossiers essentiels au projet:

  

[**src**](EntityFramework_LoL/Sources) : **Toute la partie code de l'application** (ne contient que le serveur TypeScript à l'heure actuelle sans l'implémentation du service de mail)

  

[**doc**](doc) : **Documentation de l'application** vous pourez y retrouvez nos différents Schéma et divers documentation

# Contexte  

  

Ce projet consiste en une API web reliée à une base de donnée SQLite permettant d'avoir accès à différentes données relatives à League of Legends, par exemple les différents champions. Une application MAUI est également disponible.  

  
  

## Get Started

- ### Prérequis

  

Vous devez disposer d'un environnement Asp.net Core Entity Framework Core 2.+ configuré.

  

- ### Installation

Tout d'abord, si ce n'est pas fait, clonez le dépôt de la branche **master**, pour cela copiez le lien URL du dépôt git :
```git clone https://codefirst.iut.uca.fr/git/arthur.valin/League-of-Legends_Project.git```

  

<div align = center>

![Comment cloner](doc/images/HowToClone.png)

  

</div>

- ### Comment lancer le projet ?

  

Ensuite, dans un terminal, assurez-vous que vous possédez les dépendances nécessaires, pour cela : ...

👉 [**Solution de l'application**](EntityFramework_LoL/Sources/LeagueOfLegends.sln)

# Fonctionnement

Le projet est entièrement développé en .NET, principalement .NET6 mis-à-part le client MAUI développé en .NET7. La partie base de données en gérée par l'ORM Entity Framework. L'API permet d'effectuer des opérations CRUD sur les données.

  
  

### **Schéma général du projet**

---

![Schéma d'architecture général du projet](./doc/images/LOLProjet_Diagram.png)

Le client effectue des requêtes à l'API qui expose différentes routes (/champions, /runes...). Ces routes sont gérées par des [contrôleurs](./EntityFramework_LoL/Sources/API_LoL_Project/Controllers). Ces requêtes peuvent contenir un corps en json qui sera mappé en une classée métier. Selon la requête, cette classe métier sera ensuite remappé en une entité qui sera ensuite persistée en base de données. L'API renvoie ensuite une réponse au client.

L'API effectue des requêtes côté base de données grâce à un [DBManager](./EntityFramework_LoL/Sources/Business), qui contient les différents oppérations permettant d'accéder et d'altérer les données.

### Schéma des différentes tables présentes au sein de la base de données

---

![Modèle de données](./doc/images/MLD.png)

  

- ### Comment ça marche au niveau du code ?

  

</br>

  

## Versionning

Notre API REST est atteignable à ces deux URIs ci-dessous :

* V1 - Ne dois pas être contactée et ne dispose pas de toutes les fonctionnalités - [V1](https://codefirst.iut.uca.fr/containers/arthurvalin-lolcontainer/api/v1/)

* **V2** - C'est sur cette URI que il faut contacter l'API - [V2](https://codefirst.iut.uca.fr/containers/arthurvalin-lolcontainer/api/v2/)


</br>

## Chaîne de connexion

Nous injectons la chaîne de connexion depuis [appsettings.json](./EntityFramework_LoL/Sources/API_LoL_Project/appsettings.json), le fichier de configuration de l'API. Il est ainsi possible de choisir un système de gestion de base de données différent en [production](./EntityFramework_LoL/Sources/API_LoL_Project/appsettings.Production.json) qu'en [développement](./EntityFramework_LoL/Sources/API_LoL_Project/appsettings.Development.json).
  
  Nous avons cependant choisis SQLite pour ces deux environnements. L'idée est de faciliter un éventuel changement de système.

## RoadMap

**Récapitulatif de notre avancée sur le projet** : 👇

<u>Où nous en sommes </u>:grey_question::grey_exclamation: ([x] &nbsp: réalisé, :warning: presque abouti, :x: non commencé )


</br>

  

:information_source:  ...

  

- [x] &nbsp; ![Dto](EntityFramework_LoL/Sources/DTO)

- [x] &nbsp; ![Mappeur](EntityFramework_LoL/Sources/ApiMappeur)

* Toutes les données qu'expose l'API sont des DTOs mappés par ces mapper [**Mappeur**](./).

- [x] &nbsp; ![Code de Retour](./)

* Les réponse de l'API respectent les normes de code de retour. Vous pouvez retrouver ici un tableau les récapitulant [**Code de Retour**](./).

- [x] &nbsp; ![Manager EF](EntityFramework_LoL/Sources/EntityMappers)

- [x] &nbsp; ![Test Unitaires EF](EntityFramework_LoL/Sources/TestEF)

- [x] &nbsp; ![Test Unitaires Api](EntityFramework_LoL/Sources/Test_Api)

- [x] &nbsp; ![Versioning]

- [x] &nbsp; ![Controller](EntityFramework_LoL/Sources/API_LoL_Project/Controllers/version2)

* Toutes les opérations crud on été réalisées avec l'inclusion de réponse HATEOS.

- [x] &nbsp; ![Liason Bdd](./)

- [x] &nbsp; ![Client Console]()

:warning ![Client MAUI http](EntityFramework_LoL/Sources/ApiLib)
* Etant donné que l'on ne pouvait pas faire compiler le client sans les dépendances, nous avons fait le client en console vous pouvez le retrouvez ici - De plus étant donné que le model client est le même que ceului api, il était logique d'utiliser les même mappeur et les DTO c'est pour sa que le cient à la référence au Mappeur API et au DTO API **[Bibliothèque du Client http ](EntityFramework_LoL/Sources/ApiLib)**.
- [x] &nbsp; ![Déploiment & Hébergement Docker](./drone/yml)
- [x] &nbsp; ![Sécurité](EntityFramework_LoL/Sources/API_LoL_Project/Middleware/Auth/AuthMiddlewareFliter.cs) 
* Minimal Sécurité juste sur les actions du Controller de rune page il faut faire attention a inculure la key : "ViveC#" dans votre request . (swgagger à  été configuré pour l'autification il vous suffit de cadena pour vous authentifier) .
<div align = center>

![Comment s'authentifier sur swagger](doc/images/AuthSwag.png)

</div>
<>

</br>

## Tests

Des [tests unitaires](./EntityFramework_LoL/Sources/TestEF) ont été réalisés via xUnit, notamment sur les entités et le DBManager.


![Couverture de tests 1](./doc/images/couverture_1.PNG)

![Couverture de tests 2](./doc/images/couverture_2.PNG)
 
![Couverture de tests 3](./doc/images/couverture_3.PNG)

## Auteurs
  
<div align = center>

  

**Arthur Valin** - *arthur.valin@etu.uca.fr* - [arthur](https://codefirst.iut.uca.fr/git/arthur.valin)

**David d'Almeida** - *david.d_almeida@etu.uca.fr* - [david](https://codefirst.iut.uca.fr/git/david.d_almeida)

<div align="center">

<a href = "https://codefirst.iut.uca.fr/git/arthur.valin">

<img src="https://codefirst.iut.uca.fr/git/avatars/041c57af1e1d1e855876d8abb5f1c143?size=870" width="50" >

</a>

  

<a href = "https://codefirst.iut.uca.fr/git/david.d_almeida">

<img src="https://codefirst.iut.uca.fr/git/avatars/0f8eaaad1e26d3de644ca522eccaea7c?size=870" width="50" >

</a>

</div>

  

© PM2

</div>